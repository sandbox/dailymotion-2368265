jQuery(function ($) {

    var myUploader = new plupload.Uploader({
        runtimes: 'html5,flash,silverlight,html4',
        browse_button: 'browse_file', // you can pass in id...
        container: document.getElementById('dm_update_form'), // ... or DOM Element itself
        multipart: true,
        multi_selection: false,
        url: Drupal.settings.change_thumbnail,
        flash_swf_url: Drupal.settings.full_url + '/js/plupload.flash.swf',
        silverlight_xap_url: Drupal.settings.full_url + '/js/plupload.silverlight.xap',
        dragdrop: true,
        filters: {
            max_file_size: '1000mb',
            mime_types: [{
                title: "Image files",
                extensions: "jpg,gif,png"
            }]
        }
    });

    myUploader.init();

    myUploader.bind('FilesAdded', function (up, files) {
        myUploader.start();
    });

    myUploader.bind('Error', function (error) {
        var err = "Invalid File format.\n";
        err += "Please upload only .jpg, .Png, .gif";
        alert(err);
    });
    // before upload starts, get the value of the other fields
    // and send them with the file
    myUploader.bind('BeforeUpload', function (up) {
        myUploader.settings.multipart_params = {
            attachment_id: $('#attach_id').val(),
            action: 'action_upload_pattern'
            // add your other fields here...    
        };
    });

    //equivalent of the your "success" callback
    myUploader.bind('FileUploaded', function (up, file, ret) {
        var result = $.parseJSON(ret.response);
        if (result.error) {
            alert(result.error);
        }
        if (result.id) {
            $('#attach_id').val(result.id);
        }
        if (result.url) {
            $('img.edit-video-thumbnail').attr('src', result.url);
            $('#attach_url').val(result.url);
        }
    });
});