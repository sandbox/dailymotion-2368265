 (function ($) {
     Drupal.behaviors.sidevideo = {
         attach: function (context, settings) {
             //Checking for connections
             if (Drupal.settings.auth_status == 'BOTH_DISCONNECTED') {
                 var msg = '';
                 msg += '<div class="not-auth-main">';
                 msg += '<div class="icon"></div>';
                 msg += '<div class="msg"><p>You are not connected to an account on Dailymotion.com and Dailymotion cloud.</p><p>In order to see your videos here, go to the plug-in settings to connect your Dailymotion account(s).</p></div>';
                 msg += '<div class="link"><a href="' + Drupal.settings.base_url + '/admin/dm/config_setting">Settings</a></div>';
                 msg += '</div>';
                 jQuery("div.sidebar-main").html(msg);
             }
             //When clicking on any Tab of sidebar video
             $("div.side-video-tabs ul li a").click(function () {
                 //Adding class to selected tab and removing it from othertab
                 $("div.side-video-tabs ul li a").removeClass('active-select');
                 jQuery(this).addClass('active-select');
                 var clicked = $(this).attr('rel');
                 //Checking for DM and DMC connection
                 //Method to get videos on the bases of clicked option
                 gettingSideVideos(clicked);
             });
             $("div.side-video-tabs ul li a").eq(0).trigger('click');

             //When Typing something in Search textbox of All Dm videos
             $("input#video_title").live('blur', function () {
                 if ($(this).val() != '') {
                     getDmAllVideosBytitle('', $(this).val());
                 } else {
                     getDmAllVideos('');
                 }
             });

             //When hitting "Enter" for searching All DM videos by title;
             $("input#video_title").live('keypress', function (event) {
                 if (event.keyCode == 13) {
                     getDmAllVideosBytitle('', $(this).val());
                     event.preventDefault();
                     return false;
                 }
             });

             //When Typing something in Search textbox of DMC videos
             $("input#dmc_video_title").live('blur', function () {
                 getDMCVideos('', $(this).val());
             });

             //When hitting "Enter" for searching DMC videos by title;
             $("input#dmc_video_title").live('keypress', function (event) {
                 if (event.keyCode == 13) {
                     getDMCVideos('', $(this).val());
                     event.preventDefault();
                     return false;
                 }
             });

             //When Typing something in Search textbox of My DM videos
             $("input#my_dm_video_title").live('blur', function () {
                 getMyDMVideos('', $(this).val());
             });

             //When hitting "Enter" for searching My DM videos by title;
             $("input#my_dm_video_title").live('keypress', function (event) {
                 if (event.keyCode == 13) {
                     getMyDMVideos('', $(this).val());
                     event.preventDefault();
                     return false;
                 }
             });

             //Getting DM my videos when changing option from dropdownbox.
             $("select#video_group").live('change', function () {
                 if ($(this).val() == 'dm_cloud') {
                     getDMCVideos('', '');
                 } else if ($(this).val() == 'dm') {
                     getMyDMVideos('', '');
                 }
             })


             $('#html_container').hoverIntent({
                 over: function () {
                     var string = '';
                     $(this).find('img.dm-video-thumbnail').fadeTo(200, 0.25).end();
                     var embed_url = $(this).find('img.dm-video-thumbnail').attr('alt');
                     var title = $(this).find('img.dm-video-thumbnail').attr('title');
                     var id = $(this).find('img.dm-video-thumbnail').attr('id');
                     var views = $(this).find('span#total-views').text();
                     var desc = $(this).find('div.meta-desc').text();
                     var videoURL = $(this).find('div.video-url').text();
                     var authorname = $(this).find('span#author').text();
                     var avatar = $(this).find('span#avatar').text();
                     string += '<iframe width="236" height="200" frameborder="0" scrolling="no" src="' + embed_url + '"></iframe>';
                     string += '<span class="popbox-title">' + title + '</span>';
                     string += '<div class="video-info">';
                     if(avatar) {
                        var avatartdiv = '<img src="'+avatar+'" class="avatar">';
                     } else {
                        var avatartdiv = '';
                     }
                     if (authorname) {
                        string += '<span class="author">'+avatartdiv+'' + authorname + '</span>';
                     }
                     string += '<span class="views">' + views + '</span>';
                     if (desc != null) {
                         string += '<span class="description">' + desc + '</span>';
                     }
                     string += '</div>';
                     string += '<div class="button-container">';
                     string += '<a class="meta-insert" rel="'+ id +'" href="' + embed_url + '" onclick="return insertIntoContent(this);">Insert video into post</a>';
                     string += '<a id="drupal_copyurl_'+ id +'" class="meta-copy" href="javascript:void(0);">Copy URL</a>';
                     string += '<div id="video-url">' + embed_url + '</div>';
                     string += '</div>';
                     $(this).find("#replace-container").html(string);
                     $(this).find('span.popbox').fadeIn();
                     var copy_sel = $('.button-container a.meta-copy');
			 copy_sel.clipboard({
			     path: Drupal.settings.full_url+'/js/clipboard/jquery.clipboard.swf',
			     copy: function() {
				 var this_sel = $(this);
				 return this_sel.next().html();
			     }
			  });
                 },
                 out: function () {
                     $(this).find('img.dm-video-thumbnail').fadeTo(200, 1).end();
                     $(this).find('span.popbox').fadeOut();
                 },
                 selector: 'li',
                 timeout: 100
             });

         }
     };
 })(jQuery);

 //Method to call responsible function to get sidebar videos
 function gettingSideVideos(lookingfor) {
     if (lookingfor == 'dailymotion') {
         if (Drupal.settings.auth_status == 'BOTH_CONNECTED' || Drupal.settings.auth_status == 'ONLY_DM_CONNECTED') {
             getDmAllVideos('');
         } else {
             var msg = '';
             msg += '<div class="not-auth-main">';
             msg += '<div class="icon"></div>';
             msg += '<div class="msg"><p>You are not connected to an account on Dailymotion.com and Dailymotion cloud.</p><p>In order to see your videos here, go to the plug-in settings to connect your Dailymotion account(s).</p></div>';
             msg += '<div class="link"><a href="' + Drupal.settings.base_url + '/admin/dm/config_setting">Settings</a></div>';
             msg += '</div>';
             jQuery("div#html_container").html(msg);
         }
     } else if (lookingfor == 'my_videos') {
         if (Drupal.settings.auth_status == 'ONLY_DM_CONNECTED') {
             getMyDMVideos('', '');
         } else {
             getDMCVideos('', '');
         }
     }
 }

 //Method to fetch DM All videos
 function getDmAllVideos(pn) {
     var node_title = jQuery('div.form-item-title').find('input[name="title"]').val();
     if (node_title != '') {
         getDmAllVideosBytitle('', node_title);
         return false;
     }
     jQuery("div#html_container").html('<img class="dm-loading" src="' + Drupal.settings.base_url + '/sites/all/modules/dailymotion/img/495.GIF" alt="loading..." />');
    // jQuery("div.overlay").fadeIn();
     // jQuery.getJSON('http://freegeoip.net/json/', function (location) {
     var pageno = (pn == '') ? 1 : pn;
     //var countryCode = location.country_code.toLowerCase();
     DM.api('/videos?filters=creative-official', {
         page: pageno,
         limit: 5,
         fields: "id,title,url,embed_url,thumbnail_url,views_total,description,owner.screenname,owner.avatar_25_url"
         // country: countryCode
     }, function (response) {
         //alert(JSON.stringify(response));
         var for_author_name = (JSON.parse(JSON.stringify(response)));
         //jQuery("div.overlay").fadeOut();
         var html = '';
         var paging = '';
         html += '<div class="side-main">';
         html += '<div class="header">';
         html += '<div class="filter"><input type="text" id="video_title" placeholder="Search Dailymotion.com" /></div>';
         html += '</div>';
         html += '<div class="content">';
         if ((response.list.length) > 0) {
             html += '<ul>';
             for (var i = 0; i < response.list.length; i++) {
                 var description = response.list[i].description;
                 if (description.length > 160) {
                     description = (response.list[i].description).substr(0, 160) + '...';
                 }

                 if (response.list[i].thumbnail_url != '' && response.list[i].thumbnail_url != null) {
                     var thumburl = response.list[i].thumbnail_url;
                 } else {
                     var thumburl = Drupal.settings.base_url + '/sites/all/modules/dailymotion/img/no_files_found.jpg';
                 }
                 html += '<li>';
                 html += '<div class="meta-image">';
                 html += '<a rel="' + response.list[i].id + '" href="' + response.list[i].embed_url + '" onclick="return insertIntoContent(this);">Insert</a>';
                 html += '<img alt="' + response.list[i].embed_url + '" id="' + response.list[i].id + '" class="dm-video-thumbnail dmMetaThumb" src="' + thumburl + '" title="' + response.list[i].title + '" />';
                 html += '</div>';
                 html += '<div class="video-info">';
                 html += '<div class="title">' + (response.list[i].title).substr(0, 25) + '...</div>';
                 html += '<span id="avatar">' + for_author_name["list"][i]["owner.avatar_25_url"] + '</span>';
                 html += '<span id="author">By ' + for_author_name["list"][i]["owner.screenname"] + '</span>';
                 html += '<span class="views" id="total-views"> ' + response.list[i].views_total + ' views</span> ';
                 html += '</div>';
                 html += '<span class="popbox"><span id="replace-container"></span><span class="tooltip-arrow"></span></span>';
                 html += '<div class="meta-desc">' + description + '</div>';
                 html += '<div class="video-url">' + response.list[i].url + '</div>';
                 html += '</li>';
             }
             html += '</ul>';
         } else if (response.error) {
             txt = "There was an error on this page.\n\n";
             txt += "Error description: " + response.error.message + "\n\n";
             txt += "Click OK to continue.\n\n";
             alert(txt);
         } else {
             html += '<div class="no-result-main">';
             html += '<div class="inner"></div>';
             html += '<div class="msg">No videos found.</div>';
             html += '</div>';
         }
         html += '</div>';
         html += '<div class="footer">';
         html += '<div class="paging">';
         if (pageno > 1) {
             var previous = parseInt(pageno) - parseInt(1);
             html += '<a class="previous" href="javascript:void(0);" onclick="getDmAllVideos(\'' + previous + '\');">Previous</a>';
         }
         if (response.has_more) {
             var next = parseInt(pageno) + parseInt(1);
             html += '<a class="next" href="javascript:void(0);" onclick="getDmAllVideos(\'' + next + '\');">Next</a>';
         }
         html += '</div>';
         html += '</div>';
         html += '</div>';
         jQuery("div#html_container").html(html);

     });
     //});
 }

 //Method to fetch DM All videos Search by title
 function getDmAllVideosBytitle(pn, title) {
     jQuery("div#html_container").html('<img class="dm-loading" src="' + Drupal.settings.base_url + '/sites/all/modules/dailymotion/img/495.GIF" alt="loading..." />');
     jQuery("div.overlay").fadeIn();
     //jQuery.getJSON('http://freegeoip.net/json/', function (location) {

     var pageno = (pn == '') ? 1 : pn;
     //var countryCode = location.country_code.toLowerCase();
     DM.api('/videos?sort=relevance', {
         page: pageno,
         limit: 5,
         fields: "id,title,url,embed_url,thumbnail_url,views_total,description,owner.screenname,owner.avatar_25_url",
         //  country: countryCode,
         search: title
     }, function (response) {
         jQuery("div.overlay").fadeOut();
         //alert(JSON.stringify(response));
         var for_author_name = (JSON.parse(JSON.stringify(response)));
         var html = '';
         var paging = '';
         html += '<div class="side-main">';
         html += '<div class="header">';
         html += '<div class="filter"><input type="text" id="video_title" value="' + title + '" placeholder="Search Dailymotion.com" /></div>';
         html += '</div>';
         html += '<div class="content">';
         if ((response.list.length) > 0) {
             html += '<ul>';
             for (var i = 0; i < response.list.length; i++) {
                 var description = response.list[i].description;
                 if (description.length > 160) {
                     description = (response.list[i].description).substr(0, 160) + '...';
                 }

                 if (response.list[i].thumbnail_url != '' && response.list[i].thumbnail_url != null) {
                     var thumburl = response.list[i].thumbnail_url;
                 } else {
                     var thumburl = Drupal.settings.base_url + '/sites/all/modules/dailymotion/img/no_files_found.jpg';
                 }
                 html += '<li>';
                 html += '<div class="meta-image">';
                 html += '<a rel="' + response.list[i].id + '" href="' + response.list[i].embed_url + '" onclick="return insertIntoContent(this);">Insert</a>';
                 html += '<img alt="' + response.list[i].embed_url + '" id="' + response.list[i].id + '" class="dm-video-thumbnail dmMetaThumb" src="' + thumburl + '" title="' + response.list[i].title + '" />';
                 html += '</div>';
                 html += '<div class="video-info">';
                 html += '<div class="title">' + (response.list[i].title).substr(0, 25) + '...</div>';
                 html += '<span id="avatar">' + for_author_name["list"][i]["owner.avatar_25_url"] + '</span>';
                 html += '<span id="author" class="author">By ' + for_author_name["list"][i]["owner.screenname"] + '</span>';
                 html += '<span class="views" id="total-views"> ' + response.list[i].views_total + ' views</span> ';
                 html += '</div>';
                 html += '<span class="popbox"><span id="replace-container"></span><span class="tooltip-arrow"></span></span>';
                 html += '<div class="meta-desc">' + description + '</div>';
                 html += '<div class="video-url">' + response.list[i].url + '</div>';
                 html += '</li>';
             }
             html += '</ul>';
         } else if (response.error) {
             txt = "There was an error on this page.\n\n";
             txt += "Error description: " + response.error.message + "\n\n";
             txt += "Click OK to continue.\n\n";
             alert(txt);
         } else if (title != "") {
             html += '<div class="no-result-main">';
             html += '<div class="inner"></div>';
             html += '<div class="msg-line-one">No videos found for <span class="italic">' + title + '</span>.<span>Try a new search.</span></div>';
             html += '</div>';
         }
         html += '</div>';
         html += '<div class="footer">';
         html += '<div class="paging">';
         if (pageno > 1) {
             var previous = parseInt(pageno) - parseInt(1);
             html += '<a class="previous" href="javascript:void(0);" onclick="getDmAllVideosBytitle(\'' + previous + '\', \'' + title + '\');">Previous</a>';
         }
         if (response.has_more) {
             var next = parseInt(pageno) + parseInt(1);
             html += '<a class="next" href="javascript:void(0);" onclick="getDmAllVideosBytitle(\'' + next + '\', \'' + title + '\');">Next</a>';
         }
         html += '</div>';
         html += '</div>';
         html += '</div>';
         html += '</div>';
         jQuery("div#html_container").html(html);
     });
     //});
 }

 //Method to get DMC videos
 function getDMCVideos(pn, title) {
     jQuery("div#html_container").html('<img class="dm-loading" src="' + Drupal.settings.base_url + '/sites/all/modules/dailymotion/img/495.GIF" alt="loading..." />');
     jQuery("div.overlay").fadeIn();
     var pageno = (pn == '') ? 1 : pn;
     jQuery.ajax({
         url: Drupal.settings.base_url + '/get-dmc-video',
         type: 'POST',
         dataType: 'json',
         data: 'title=' + title + '&pageno=' + pageno,
         success: function (response) {
             jQuery("div.overlay").fadeOut();
             var html = '';
             html += '<div class="side-main">';
             //Header starts
             html += '<div class="header">';
             html += '<div class="filter">';

             if (Drupal.settings.auth_status == 'BOTH_CONNECTED' || Drupal.settings.auth_status == 'ONLY_DM_CONNECTED') {
                 html += '<select id="video_group">';
                 html += '<option value="dm_cloud" selected="selected">Dailymotion Cloud</option>';
                 html += '<option value="dm">Dailymotion.com</option>';
                 html += '</select>';
             }
             if (response != "" || title != "") {
                 html += '<input type="text" id="dmc_video_title" value="' + title + '" placeholder="Search" />';
             }
             html += '</div>';
             html += '</div>';
             //Header ends


             if ((response == "" && title == "")) {
                 html += '<div class="no-video-main">';
                 html += '<div class="icon"></div>';
                 html += '<div class="msg"><p>You have not uploaded any video yet.</p><p>Start uploading your videos now!</p></div>';
                 html += '<div class="link"><a href="' + Drupal.settings.base_url + '/admin/dm/video_upload">Upload videos</a></div>';
                 html += '</div>';
             } else {
                 html += '<div class="content">';

                 if (typeof response.videos != "undefined" && (response.videos instanceof Array)) {
                     html += '<ul>';
                     for (var i = 0; i < response.videos.length; i++) {
                         if (response.videos[i].stream_url != '' && response.videos[i].stream_url != null) {
                             var thumburl = response.videos[i].stream_url;
                         } else {
                             var thumburl = Drupal.settings.base_url + '/sites/all/modules/dailymotion/img/no_files_found.jpg';
                         }
                         html += '<li>';
                         html += '<div class="meta-image">';
                         html += '<a rel="' + response.videos[i].media_id + '" href="' + response.videos[i].embed_url + '" onclick="return insertIntoContent(this);">Insert</a>';
                         html += '<img alt="' + response.videos[i].embed_url + '" id="' + response.videos[i].media_id + '" class="dm-video-thumbnail dmMetaThumb" src="' + thumburl + '" title="' + response.videos[i].title + '" />';
                         html += '</div>';
                         html += '<div class="video-info">';
                         html += '<div class="title">' + (response.videos[i].title).substr(0, 37) + '...</div>';
                         //html += '<span class="author">By ' + response.list[i].owner.screenname + '</span>';
                         html += '<span class="views" id="total-views"> ' + response.videos[i].total_view + ' views</span> ';
                         html += '</div>';
                         html += '<span class="popbox"><span id="replace-container"></span><span class="tooltip-arrow"></span></span>';
                         //html += '<div class="video-url">' +response.videos[i].url+ '</div>';
                         html += '</li>';
                     }
                     html += '</ul>';
                 } else if (response.error) {
                     txt = "There was an error on this page.\n\n";
                     txt += "Error description: " + response.error.message + "\n\n";
                     txt += "Click OK to continue.\n\n";
                     alert(txt);
                 } else if (title != "") {
                     html += '<div class="no-result-main">';
                     html += '<div class="inner"></div>';
                     html += '<div class="msg-line-one">No videos found for <span class="italic">' + title + '</span>.<span>Try a new search.</span></div>';
                     html += '</div>';
                 }

                 html += '</div>';
                 html += '<div class="footer">';
                 html += '<div class="paging">';
                 if (pageno > 1) {
                     var previous = parseInt(pageno) - parseInt(1);
                     html += '<a class="previous" href="javascript:void(0);" onclick="getDMCVideos(\'' + previous + '\', \'' + title + '\');">Previous</a>';
                 }
                 if (response.total_pages > pageno) {
                     var next = parseInt(pageno) + parseInt(1);
                     html += '<a class="next" href="javascript:void(0);" onclick="getDMCVideos(\'' + next + '\', \'' + title + '\');">Next</a>';
                 }
                 html += '</div>';
                 html += '</div>';
             }

             html += '</div>';
             jQuery("div#html_container").html(html);

         },
         beforeSend: function () {
             //jQuery("div.overlay").show();
             //jQuery("div.loading-image-container").show();
         },
         error: function () {
             alert("Something went wrong.");
         }
     });
 }

 //Method to get My DM videos
 function getMyDMVideos(pn, title) {
     jQuery("div#html_container").html('<img class="dm-loading" src="' + Drupal.settings.base_url + '/sites/all/modules/dailymotion/img/495.GIF" alt="loading..." />');
     jQuery("div.overlay").fadeIn();
     var pageno = (pn == '') ? 1 : pn;
     jQuery.ajax({
         url: Drupal.settings.base_url + '/get-dm-video',
         type: 'POST',
         dataType: 'json',
         data: 'title=' + title + '&pageno=' + pageno,
         success: function (response) {
             jQuery("div.overlay").fadeOut();
             var html = '';
             html += '<div class="side-main">';

             html += '<div class="header">';
             html += '<div class="filter">';

             if (Drupal.settings.auth_status == 'BOTH_CONNECTED' || Drupal.settings.auth_status == 'ONLY_DMC_CONNECTED') {
                 html += '<select id="video_group">';
                 html += '<option value="dm_cloud">Dailymotion Cloud</option>';
                 html += '<option value="dm" selected="selected">Dailymotion.com</option>';
                 html += '</select>';
             }
             if (response != "" || title != "") {
                 html += '<input type="text" id="my_dm_video_title" value="' + title + '" placeholder="Search" />';
             }
             html += '</div>';
             html += '</div>';

             if (response == "" && title == "") {
                 jQuery("input#my_dm_video_title").hide();
                 html += '<div class="no-video-main">';
                 html += '<div class="icon"></div>';
                 html += '<div class="msg"><p>You have not uploaded any video yet.</p><p>Start uploading your videos now!</p></div>';
                 html += '<div class="link"><a href="' + Drupal.settings.base_url + '/admin/dm/video_upload">Upload videos</a></div>';
                 html += '</div>';
             } else {
                 html += '<div class="content">';

                 if (typeof response.videos != "undefined" && (response.videos instanceof Array)) {
                     html += '<ul>';
                     for (var i = 0; i < response.videos.length; i++) {
                         if (response.videos[i].thumbnail_url != '' && response.videos[i].thumbnail_url != null) {
                             var thumburl = response.videos[i].thumbnail_url;
                         } else {
                             var thumburl = Drupal.settings.base_url + '/sites/all/modules/dailymotion/img/no_files_found.jpg';
                         }
                         html += '<li>';
                         html += '<div class="meta-image">';
                         html += '<a rel="' + response.videos[i].id + '" href="' + response.videos[i].embed_url + '" onclick="return insertIntoContent(this);">Insert</a>';
                         html += '<img alt="' + response.videos[i].embed_url + '" id="' + response.videos[i].id + '" class="dm-video-thumbnail dmMetaThumb" src="' + thumburl + '" title="' + response.videos[i].title + '" />';
                         html += '</div>';
                         html += '<div class="video-info">';
                         html += '<div class="title">' + (response.videos[i].title).substr(0, 37) + '...</div>';
                         //html += '<span class="author">By ' + response.list[i].owner.screenname + '</span>';
                         html += '<span class="views" id="total-views"> ' + response.videos[i].views_total + ' views</span> ';
                         html += '</div>';
                         html += '<span class="popbox"><span id="replace-container"></span><span class="tooltip-arrow"></span></span>';
                         // html += '<div class="video-url">' +response.videos[i].url+ '</div>';
                         html += '</li>';
                     }
                     html += '<ul>';
                 } else if (response.error) {
                     txt = "There was an error on this page.\n\n";
                     txt += "Error description: " + response.error.message + "\n\n";
                     txt += "Click OK to continue.\n\n";
                     alert(txt);
                 } else if (title != "") {
                     html += '<div class="no-result-main">';
                     html += '<div class="inner"></div>';
                     html += '<div class="msg-line-one">No videos found for <span class="italic">' + title + '</span>.<span>Try a new search.</span></div>';
                     html += '</div>';
                 }

                 html += '</div>';
                 html += '<div class="footer">';
                 html += '<div class="paging">';
                 if (pageno > 1) {
                     var previous = parseInt(pageno) - parseInt(1);
                     html += '<a class="previous" href="javascript:void(0);" onclick="getMyDMVideos(\'' + previous + '\', \'' + title + '\');">Previous</a>';
                 }
                 if (response.has_more) {
                     var next = parseInt(pageno) + parseInt(1);
                     html += '<a class="next" href="javascript:void(0);" onclick="getMyDMVideos(\'' + next + '\', \'' + title + '\');">Next</a>';
                 }
                 html += '</div>';
                 html += '</div>';
             }

             html += '</div>';
             jQuery("div#html_container").html(html);

         },
         beforeSend: function () {
             //jQuery("div.overlay").show();
             //jQuery("div.loading-image-container").show();
         },
         error: function () {
             alert("Something went wrong.");
         }
     });
 }

 //Logic to inserting creating short code and inserting into body content
 function insertIntoContent(obj) {
     var media_url = obj.getAttribute("href");
     var media_id = obj.getAttribute("rel");
     var width = 300;
     var height = 250;
     var html = '[dm EMURL=' + media_url + ' WD=' + width + ' HE=' + height + ' VOID=' + media_id + '][/dm]';
     if (jQuery('textarea#edit-body-und-0-value').length) {
         insertAtCaret('edit-body-und-0-value', html);
     } else {
         alert('Content area does not exist for inserting video.');
         return false;
     }
     return false;
 }

 function insertAtCaret(areaId, text) {
     var txtarea = document.getElementById(areaId);
     var scrollPos = txtarea.scrollTop;
     var strPos = 0;
     var br = ((txtarea.selectionStart || txtarea.selectionStart == '0') ?
         "ff" : (document.selection ? "ie" : false));
     if (br == "ie") {
         txtarea.focus();
         var range = document.selection.createRange();
         range.moveStart('character', -txtarea.value.length);
         strPos = range.text.length;
     } else if (br == "ff") strPos = txtarea.selectionStart;

     var front = (txtarea.value).substring(0, strPos);
     var back = (txtarea.value).substring(strPos, txtarea.value.length);
     txtarea.value = front + text + back;
     strPos = strPos + text.length;
     if (br == "ie") {
         txtarea.focus();
         var range = document.selection.createRange();
         range.moveStart('character', -txtarea.value.length);
         range.moveStart('character', strPos);
         range.moveEnd('character', 0);
         range.select();
     } else if (br == "ff") {
         txtarea.selectionStart = strPos;
         txtarea.selectionEnd = strPos;
         txtarea.focus();
     }
     txtarea.scrollTop = scrollPos;
 }