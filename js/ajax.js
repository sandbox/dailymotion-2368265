 (function ($) {
     Drupal.behaviors.dailymotion = {
         attach: function (context, settings) {
             //Logic to show Dailymotion and Cloud videos on changing from Select field
             $('#dm_group').change(function () {
                 showContentVideos(this.value, '');
             });
             //Logic to shop popbox while hovering on thumbnails showing in block
             $(".content-container").hoverIntent({
                 over: function () {
                     var title = $(this).find('img').attr('title');
                     var embed_url = $(this).find('img').attr('alt');
                     var media_Id = $(this).attr('id');
                     var popbox = '';
                     popbox += '<div class="dm-popbox dm-common">';
                     popbox += '<div class="title">' + title + '</div>';
                     popbox += '<div class="iframe"><img src="' + Drupal.settings.base_url + '/sites/all/modules/dailymotion/img/495.GIF" alt="Loading..." /><iframe width="236" height="200" frameborder="0" scrolling="no" src="' + embed_url + '" onload="hidePopboxLoading();"></iframe></div>';
                     popbox += '<div class="insert-link" onclick="insertIntoContent(\'' + media_Id + '\')">Insert into content</div>';
                     popbox += '</div>';
                     $(this).find("#replace-container").html(popbox);
                     $(this).find('span.popbox').fadeIn();
                 },
                 out: function () {
                     $(this).find('span.popbox').fadeOut();
                 },
                 selector: 'li',
                 timeout: 100
             });
             //Logic to block page refresh while entering title in search box and press enter 
             $('#search_title').keypress(function (event) {
                 if (event.keyCode == 13) {
                     event.preventDefault();
                     return false;
                 }
             });
             //Logic to display search result while search through search title in block.
             $("#search_title").blur(function () {
                 var title = $(this).val();
                 var group = $("#dm_group").val();
                 alert(group);
                 if (title != '' && group != '') {
                     if (group == 'dmc') {
                         showDMAllVideosByTitle(title, 1);
                     } else if (group == 'dm') {
                         showDMMyVideosByTitle(title, 1);
                     }
                 }
             });
                        //Logic to show delete confirm box when clicked on Trash link on DM listing page.
             jQuery(".trash-trigger").bind("click", function () {
                 jQuery("div.confirm-box").hide();
                 jQuery(this).closest("div").find("div.confirm-box").fadeIn();
             });
             //Logic to hide Delete confirm box of Dailymotion Video Gallery when clicked on Keep-it link
             jQuery("a.keep-it").live("click", function () {
                 jQuery("div.confirm-box").fadeOut();
             });
             //Logic to show priveiw while click anywhere on row
             jQuery("tr.dm-gallery-rows").click(function (event) {
                 if (!jQuery(event.target).is('a')) {
                     try {
                         var embedUrl = jQuery(this).find('img').attr('alt');
			 embedUrl = embedUrl+Drupal.settings.parameter;
                         var duration = jQuery(this).find('td.third-column').find('div#duration').text();
                         var title = jQuery(this).find('img').attr('title');
                         var views = jQuery(this).find('div.views-channel').find("span.views").text();
                         var status = jQuery(this).find('td.third-column').find('div#video_status').text();
                         var desc = jQuery(this).find('td.third-column').find('div#desc').text();
                         var tags = jQuery(this).find('td.third-column').find('div.tags').text();
                         string = '';
                         string += '<div class="dm-preview-container dm-common">';
                         string += '<iframe width="450" height="250" frameborder="0" scrolling="no" src="' + embedUrl + '"></iframe>';
                         string += '<div class="inner">';
                         string += '<div class="title">' + title + '</div>';
                         string += '<div class="tag-view"><span class="duration">' + secondsTimeSpanToHMS(duration) + '</span> - <span class="two italic">' + views + '</span></div>';
                         string += '<div class="publish"><span class="logo"></span> - ' + status + '</div>';
                         string += '<div class="break"></div>';
                         string += '<div class="desc">' + desc + '</div>';
                         string += '<div class="embed-url"><a href="' + embedUrl.replace('embed/', '') + '" target="_blank">' + embedUrl.replace('embed/', '') + '</a></div>';
                         string += '</div>';
                         string += '</div>';
                         jQuery.fancybox(string, {
                             overlayShow: true,
                             hideOnContentClick: false
                         });
                     } catch (err) {
                         txt = "There was an error on this page.\n\n";
                         txt += "Error description: " + err.message + "\n\n";
                         txt += "Click OK to continue.\n\n";
                         alert(txt);
                     }
                 }
             });

             //Logic to display video preview while clicking on entire row on DMC gallery page
             jQuery("tr.dmc-gallery-rows").click(function (event) {
                 if (!jQuery(event.target).is('a')) {
                     try {
                         var embedUrl = jQuery(this).find('img').attr('alt');
                         var duration = jQuery(this).find('div.other-backend-info').find("div#duration").text();
                         var title = jQuery(this).find('img').attr('title');
                         var views = jQuery(this).find("div.views").text();
                         string = '';
                         string += '<div class="dm-preview-container dm-common">';
                         string += '<iframe width="450" height="250" frameborder="0" scrolling="no" src="' + embedUrl + '"></iframe>';
                         string += '<div class="inner">';
                         string += '<div class="title">' + title + '</div>';
                         string += '<div class="views"><span class="duration">' + secondsTimeSpanToHMS(duration) + '</span> - <span class="italic">' + views + '</span></div>';
                         string += '<div class="logo"></div>';
                         string += '</div>';
                         string += '</div>';
                         jQuery.fancybox(string, {
                             overlayShow: true,
                             hideOnContentClick: false
                         });
                     } catch (err) {
                         txt = "There was an error on this page.\n\n";
                         txt += "Error description: " + err.message + "\n\n";
                         txt += "Click OK to continue.\n\n";
                         alert(txt);
                     }
                 }
             });
             //Logic to display preview of Dailymotion Cloud video when click on "view" link.
             jQuery(".view-trigger").bind("click", function () {
                 jQuery(this).closest("tr.dm-gallery-rows").trigger("click");
             });
             //Logic to display preview of Dailymotion Cloud video when click on "view" link.
             jQuery(".view-trigger").bind("click", function () {
                 jQuery(this).closest("tr.dmc-gallery-rows").trigger("click");
             });

             //Logic to validate metatags values for Dailymotion cloud gallery videos on edit page
             jQuery("a#dmc-new-tag").live('click', function () {
                 var error = '';
                 var metaValue = jQuery(this).prev('input').val();
                 var keyValue = jQuery(this).prev().prev('input').val();
                 jQuery(".alert-msg").hide();
                 jQuery(this).prev('input').removeClass('missing-value');
                 jQuery(this).prev().prev('input').removeClass('missing-value');
                 if (metaValue != '' || keyValue != '') {
                     if (keyValue != '') {
                         if (isNumeric(keyValue)) {
                             error = 'NUMERIC';
                         } else if (!(metaValue)) {
                             error = 'CORRES_VALUE_NULL';
                         }
                     } else {
                         error = 'KEY_EMPTY';
                     }
                     if (error == 'NUMERIC') {
                         jQuery("#dmc-message").html("Meta key should not be numeric.").show();
                         jQuery(this).prev().prev('input').addClass('missing-value');
                         return false;
                     } else if (error == 'CORRES_VALUE_NULL') {
                         jQuery("#dmc-message").html("Value against to entered meta key should not be empty.").show();
                         jQuery(this).prev('input').addClass('missing-value');
                         return false;
                     } else if (error == 'KEY_EMPTY') {
                         jQuery("#dmc-message").html("Meta key value should not be empty.").show();
                         jQuery(this).prev().prev('input').addClass('missing-value');
                         return false;
                     }
                     var tagcounter = 2;
                     var counter = jQuery('#counter-value').val();
                     if (counter) {
                         tagcounter = counter;
                     }
                     tagcounter++;
                     jQuery('#counter-value').val(tagcounter);
                     if (tagcounter > 5) {
                         jQuery("#dmc-message").html("You can not add more tags.").show();
                         return false;
                     }
                     var html = '';
                     html += '<div class="tag" id="meta_' + keyValue + '">';
                     html += '<label>' + keyValue + ':</label>';
                     html += '<input type="hidden" value="' + keyValue + '" name="meta[val' + tagcounter + '][]" class="keyInput" size="50">';
                     html += '<input type="text" value="' + metaValue + '" name="meta[val' + tagcounter + '][]">';
                     html += '<a href="javascript:void(0);" onclick="deleteMetatags(\'\', \'' + keyValue + '\');" class="delete-tag">Remove</a>';
                     html += '</div>';
                     jQuery("div.present-tags").append(html);
                     jQuery(this).prev('input').val('');
                     jQuery(this).prev().prev('input').val('');
                 } else {
                     jQuery("#dmc-message").html("Some information is missing. We've heighlighted the fields for you.").show();
                     if (metaValue == '' && keyValue == '') {
                         jQuery(this).prev('input').addClass('missing-value');
                         jQuery(this).prev().prev('input').addClass('missing-value');
                     } else if (keyValue == '') {
                         jQuery(this).prev().prev('input').addClass('missing-value');
                     } else if (metaValue == '') {
                         jQuery(this).prev('input').addClass('missing-value');
                     }
                 }
             });

             //Logic to display Delete confirm box on Dailymotion Cloud Video Gallery
             jQuery(".dmc-trash-trigger").live("click", function () {
                 jQuery("div.confirm-box").hide();
                 jQuery(this).closest("div").find("div.confirm-box").fadeIn();
             });
             //Logic to hide Delete confirm box of Dailymotion Cloud Video Gallery when clicked on Keep-it link
             jQuery("a.dmc-keep-it").live("click", function () {
                 jQuery("div.confirm-box").fadeOut();
             });

             //Logic to blur altername option content while clicking on private and public radio buttons
             jQuery("#dm-video-private").live("click", function () {
                 jQuery(this).closest("div.visibility-wrap").removeClass("blur");
                 jQuery("div.visibility_public").addClass("blur");
             });
             jQuery("#dm-video-public").live("click", function () {
                 jQuery(this).closest("div.visibility-wrap").removeClass("blur");
                 jQuery("div.visibility_private").addClass("blur");
             });
         }
     }
 })(jQuery);

 // jQuery function to edit Dailymotion cloud video
 function editDailymotionvideo(mediaId) {
    jQuery("div.dm-loading").fadeIn();
     jQuery.ajax({
         url: Drupal.settings.base_url + '/edit-dm-video',
         type: 'POST',
         dataType: 'json',
         data: 'mediaId=' + mediaId,
         success: function (data) {
	     jQuery("div.dm-loading").fadeOut();
             jQuery.fancybox(data, {
                 overlayShow: true,
                 hideOnContentClick: false
             });
             jQuery('#video-tags').tagsInput({
                 width: 'auto'
             });
         },
         beforeSend: function () {
         },
         error: function () {
             alert("Something went wrong.");
         }
     });
 }
 //Method to update/save DM edit form on DM gallery page.
 function getDailymotionupdatedvalues() {
     jQuery("div.title-wrap").removeClass("missing-value");
     jQuery("div.channel-wrap").removeClass("missing-value");

     if (jQuery("#video-title").val() == '') {
         jQuery("div.title-wrap").addClass("missing-value");
         jQuery("#dm-message").html("Some information is missing. We've highlighted the fields above for you.").show();
         return false;
     }
     if (jQuery("#channel").val() == '') {
         jQuery("div.channel-wrap").addClass("missing-value");
         jQuery("#dm-message").html("Some information is missing. We've highlighted the fields above for you.").show();
         return false;
     }
     var counterdata = jQuery('#dm_update_form').serialize();
     jQuery.ajax({
         url: Drupal.settings.base_url + '/update-dm-data',
         type: 'POST',
         dataType: 'html',
         data: counterdata,
         success: function (data) {
             if (data) {
                 jQuery("#dm-message").addClass('success-msg');
                 jQuery("#dm-message").html("Video data update successfully.").show();
                 setTimeout("location.reload(true);", 2000);
             }
         },
         error: function () {
             jQuery("#dm-message").html("Something went wrong.").show();
         }
     });
 }

 // jQuery function to edit Dailymotion cloud video
 function editDMCvideo(mediaId) {
     jQuery("div.dm-loading").fadeIn();
     jQuery.ajax({
         url: Drupal.settings.base_url + '/edit-dmc-video',
         type: 'POST',
         dataType: 'json',
         data: 'mediaId=' + mediaId,
         success: function (data) {
	     jQuery("div.dm-loading").fadeOut();
             jQuery.fancybox(data, {
                 overlayShow: true,
                 hideOnContentClick: false
             });
             jQuery('#video-tags').tagsInput({
                 width: 'auto'
             });
         },
         beforeSend: function () {
         },
         error: function () {
             alert("Something went wrong.");
         }
     });
 }


 function getDMCupdatedvalues() {
     //Validating key values
     var error = '';
     if (jQuery("#video-title").val() == '') {
         jQuery("#video-title").addClass('missing-value');
         jQuery("#dmc-message").html("Video title should not be empty.").show();
         return false;
     }
     jQuery(".keyInput").each(function () {
         var value = jQuery(this).val();
         if (value != '' || jQuery(this).next('input').val() != '') {
             if (value != '') {
                 if (isNumeric(value)) {
                     error = 'NUMERIC';
                 } else if (!(jQuery(this).next('input').val())) {
                     error = 'CORRES_VALUE_NULL';
                 }
             } else {
                 error = 'KEY_EMPTY';
             }
         }
     });
     if (error == 'NUMERIC') {
         jQuery("#dmc-message").html("Meta key should not be numeric.").show();
         return false;
     } else if (error == 'CORRES_VALUE_NULL') {
         jQuery("#dmc-message").html("Value against to entered meta key should not be empty.").show();
         return false;
     } else if (error == 'KEY_EMPTY') {
         jQuery("#dmc-message").html("Meta key value should not be empty.").show();
         return false;
     }
     var tags = '';
     var counterdata = jQuery('#dm_update_form').serialize();
     jQuery.ajax({
         url: Drupal.settings.base_url + '/update-dmc-video',
         type: 'POST',
         dataType: 'json',
         data: counterdata,
         success: function (data) {
             if (data) {
                 jQuery("#dmc-message").addClass('success-msg');
                 jQuery("#dmc-message").html("Video meta update successfully.").show();
                 setTimeout("location.reload(true);", 2000);
             }
         },
         error: function () {
             jQuery("#dmc-message").html("Something went wrong.").show();
         }
     });
 }

 //Jquery method to delete meta tags:
 function deleteMetatags(mediaId, key) {
     jQuery.ajax({
         url: Drupal.settings.base_url + '/delete-dm-cloud-metatags',
         type: 'POST',
         mediaId: mediaId,
         key: key,
         success: function (data) {
             jQuery("#meta_" + key).remove();
         },
         beforeSend: function () {
         },
         error: function () {
             alert("Something went wrong.");
         }
     });
 }

 function getQueryStringByName(name) {
     name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
     var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
         results = regex.exec(location.search);
     return results == null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
 }

 function hidePopboxLoading() {
     jQuery("div.iframe").find('img').fadeOut();
 }

 function showContentVideos(group, pn) {
     var string = '';
     var paging = '';
     var pageno = ((pn == '') ? 1 : pn);
     jQuery(".dm-block-loading").show();
     jQuery.ajax({
         type: "POST",
         url: Drupal.settings.group_videos,
         data: 'group=' + group + '&pageno=' + pageno,
         dataType: 'json',
         success: function (data) {
             if (data) {
                 if (data.videos.length > 0) {
                     string += '<div class="dailymotion-content">';
                     string += '<ul class="parent">';
                     for (var i = 0; i < data.videos.length; i++) {
                         string += '<li id="' + data.videos[i].media_id + '" class="box contentThumb"><img src="' + data.videos[i].thumbnail_url + '" alt="' + data.videos[i].embed_url + '" title="' + data.videos[i].title + '" /><span class="popbox"><span id="replace-container"></span></span></li>';
                     }
                     string += '</ul>';
                     string += '</div>';
                     if (group == 'dmc') {
                         paging += '<div class="paging">';
                         if (pageno > 1) {
                             var previous = parseInt(pageno) - parseInt(1);
                             paging += '<a href="javascript:void(0);" onclick="showContentVideos(\'' + group + '\', \'' + previous + '\');">Previous</a>';
                         }
                         if (data.total_pages > pageno) {
                             var next = parseInt(pageno) + parseInt(1);
                             paging += '<a href="javascript:void(0);" onclick="showContentVideos(\'' + group + '\', \'' + next + '\');">Next</a>';
                         }
                         paging += '</div>';
                     } else if (group == 'dm') {
                         paging += '<div class="paging">';
                         paging += '<ul>';
                         if (pageno > 1) {
                             var previous = parseInt(pageno) - parseInt(1);
                             paging += '<a href="javascript:void(0);" onclick="showContentVideos(\'' + group + '\', \'' + previous + '\');">Previous</a>';
                         }
                         if (data.has_more) {
                             var next = parseInt(pageno) + parseInt(1);
                             paging += '<a href="javascript:void(0);" onclick="showContentVideos(\'' + group + '\', \'' + next + '\');">Next</a>';
                         }
                         paging += '</ul>';
                         paging += '</div>';
                     }
                     string += paging;

                 }
                 jQuery('div.content-container').html(string);
             }
             jQuery(".dm-block-loading").hide();
         },
         error: function () {
             alert("Something went wrong.");
         }
     });
 }

 function showDMAllVideosByTitle(title, pn) {
     var pageno = ((pn == '') ? 1 : pn);
     jQuery(".dm-block-loading").show();
     jQuery.getJSON('http://freegeoip.net/json/', function (location) {
         var countryCode = location.country_code.toLowerCase();
         DM.api('/videos', {
             page: pn,
             limit: 18,
             search: title,
             fields: "id, title, embed_url, thumbnail_url",
             country: countryCode
         }, function (response) {
             var stringelement = '';
             var paging = '';
             var paging = '';
             if ((response.list.length) > 0) {
                 stringelement += '<div class="dailymotion-content">';
                 stringelement += '<ul class="parent">';
                 for (var i = 0; i < response.list.length; i++) {
                     stringelement += '<li id="' + data.videos[i].media_id + '" class="box contentThumb"><img src="' + response.list[i].thumbnail_url + '" alt="' + response.list[i].embed_url + '" title="' + response.list[i].title + '" /><span class="popbox"><span id="replace-container"></span></span></li>';
                 }
                 stringelement += '</ul>';
                 stringelement += '</div>';
             } else if (response.error) {
                 txt = "There was an error on this page.\n\n";
                 txt += "Error description: " + response.error.message + "\n\n";
                 txt += "Click OK to continue.\n\n";
                 alert(txt);
             } else {
                 stringelement += 'Opps! no video found.';
             }
             paging += '<div class="paging">';
             if (pageno > 1) {
                 var previous = parseInt(pageno) - parseInt(1);
                 paging += '<a href="javascript:void(0);" onclick="showDMAllVideosByTitle(\'' + title + '\', \'' + previous + '\');">Previous</a>';
             }
             if (response.has_more) {
                 var next = parseInt(pageno) + parseInt(1);
                 paging += '<a href="javascript:void(0);" onclick="showDMAllVideosByTitle(\'' + title + '\', \'' + next + '\');">Next</a>';
             }
             paging += '</div>';
             stringelement += paging;
             jQuery('div.content-container').html(stringelement);
             jQuery(".dm-block-loading").hide();
         });
     });
 }

 function showDMMyVideosByTitle(title, pn) {
     alert('Dailymotion My videos will go here.It is still need to be implement as auth process gets start.');
 }

 function ucwords(str) {
     return (str + '').replace(/^([a-z\u00E0-\u00FC])|\s+([a-z\u00E0-\u00FC])/g, function (jQuery1) {
         return jQuery1.toUpperCase();
     });
 }
 //Method to convers second into time
 function secondsTimeSpanToHMS(s) {
     var h = Math.floor(s / 3600); //Get whole hours
     s -= h * 3600;
     var m = Math.floor(s / 60); //Get remaining minutes
     s -= m * 60;
     if (h > 00) {
        return h + ":" + (m < 10 ? '0' + m : m) + ":" + (s < 10 ? '0' + s : s); //zero padding on minutes and seconds
     }else {
	return (m < 10 ? '0' + m : m) + ":" + (s < 10 ? '0' + s : s); //zero padding on minutes and seconds
     }
 }
 //Callback function to check numeric value
 function isNumeric(n) {
     return !isNaN(parseFloat(n)) && isFinite(n);
 }