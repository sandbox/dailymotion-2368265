INTRODUCTION

------------

The Dailymotion module allows you to connect your Drupal account with your Dailymotion.com and/or Dailymotion Cloud accounts. The module provides you with various functionalities such as:

Accessing your video libraries hosted on Dailymotion.com and/or Dailymotion Cloud,

 * Leveraging a Dailymotion publisher ID (http://publisher.dailymotion.com),

 * Searching and filtering videos on these libraries,

 * Editing, deleting and uploading videos on these libraries,

 * Searching any public video and include it in your content.

Integrating your content hosted on Dailymotion.com and Dailymotion Cloud has never been easier!


INSTALLATION

------------

1. Download the module and keep it in module directory sites/all/modules/
2. Enable module in module list located at administer > structure > modules.
3. Configure module in configuration setting located at administer > configuration > dailymotion.

CONFIGURATION

-------------

1. Go to configuration setting located at administer > configuration > dailymotion.
2. Click on button "Connect to Dailymotion" and/or "Connect to Dailymotion Cloud" located at administer > dailymotion > User settings for connecting Dailymotion and Dailymotion Cloud account(s) respectively.
3. Enter API key and User id of your account for connecting with your account(s).
4. Select your default channel.

FAQ

---

Q: How do I set up my Dailymotion.com and/or Dailymotion Cloud account?

A: To connect your Dailymotion and/or Dailymotion Cloud account(s), click on "User settings" under the Dailymotion navigation link displayed on your Drupal admin menu. To set up your account, fill in your user ID and your API key; both can be found below the "Profile" section on dailymotion.com or dmcloud.net under "API". In case you connect both your Dailymotion and Dailymotion Cloud accounts, please select a default channel.

Q: How do I manage (upload, edit, delete) my videos?

A: You can easily manage your videos from the Video Gallery:
- Upload your video: Click on the "Upload videos" button on the top right to upload a video to your Dailymotion.com and/or Dailymotion Cloud account regardless of the number of videos. You can also directly click on "Upload" in admin menu under "Dailymotion".
- Edit your video: Click on "Edit" - a button that appears when placing the mouse on the title; you can change the thumbnail, edit the title of your video and add tags to your video.
- Delete your video: Click on "Trash". Warning: By clicking on "Yes, delete", you permanently remove the video from your Dailymotion.com/Dailymotion Cloud account.

Q: How do I publish my video on my Drupal site?

A: When adding a new node on Drupal, the Dailymotion block - located by default on the right sidebar - automatically displays your videos; Select the video you want and click on "Insert": You can either directly insert your video into the post or copy the URL.

MAINTAINERS

-----------

Current maintainers:

 * XXXX (sun) - https://drupal.org/user/XXX

