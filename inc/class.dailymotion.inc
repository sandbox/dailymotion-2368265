<?php
include_once(DRUPAL_ROOT . "/sites/all/modules/dailymotion/lib/Dailymotion.php");

class DailymotionOwnMethod 
{
    //For create instance of daliymotion
    protected $dailymotion;
    //For retun data
    public $returndata = array( );
   
    public function __construct( )
    {
        $this->dailymotion = new Dailymotion();
        $dailymotionoption = variable_get('dailymotion_app_secret_info', '');
        $apiKey    = $dailymotionoption['dailymotion_apikey'];
        $apiSecret = $dailymotionoption['dailymotion_secretkey'];
        $this->dailymotion->setGrantType( Dailymotion::GRANT_TYPE_TOKEN, $apiKey, $apiSecret, array(
             'manage_videos' 
        ) );
    }
    
    /**
     * get assosciated chennel list from dailymotion
     */
    public function getDailymotionChannelList( )
    {
        $resultdata = array();
        try {
            $result = $this->dailymotion->get( '/channels', array(
                 'fields' => array(
                     'id',
                    'name' 
                ) 
            ) );
            if ( isset( $result['list'] ) && !empty( $result['list'] ) ) {
                foreach ( $result['list'] as $media ) {
                    $resultdata[$media['id']] = $media['name'];
                }
            }
            return ( $resultdata );
        }
        catch ( Exception $e ) {
            watchdog('dailymotion', 'Function : getDailymotionChannelList Mesg : @msg', array('@msg' => $e->getMessage()) , WATCHDOG_ERROR);
            return $resultdata['error'] = $e->getMessage();
        }
    }
    
    /**
     * Get dailymotion video lists
     */
    public function getDailymotionVideoList( $flag = NULL, $fields = array( 'id', 'title', 'embed_url', 'thumbnail_url', 'private', 'type' ), $status, $page_no = 1, $per_page = 10, $search_title )
    {
        $resultdata = array();
        switch ( $flag ) {
            case 'all':
                try {
                    if ( !empty( $search_title ) ) {
                        $result = $this->dailymotion->get( '/videos?sort=relevance', array(
                             'fields' => $fields,
                            'country' => $this->visitorCountry(),
                            'search' => $search_title,
                            'page' => ( $page_no ),
                            'limit' => ( $per_page ) 
                        ) );
                    } else {
                        $result = $this->dailymotion->get( '/videos', array(
                             'fields' => $fields,
                            'country' => $this->visitorCountry(),
                            'page' => ( $page_no ),
                            'limit' => ( $per_page ) 
                        ) );
                    }
                    if ( isset( $result['list'] ) && !empty( $result['list'] ) ) {
                        foreach ( $result['list'] as $key => $media ) {
                            $resultdata['videos'][$key]['id'] = !empty( $media['id'] ) ? $media['id'] : null;
                            $resultdata['videos'][$key]['title'] = !empty( $media['title'] ) ? $media['title'] : null;
                            $resultdata['videos'][$key]['embed_url'] = !empty( $media['embed_url'] ) ? $media['embed_url'] : null;
                            $resultdata['videos'][$key]['thumbnail_url'] = !empty( $media['thumbnail_url'] ) ? $media['thumbnail_url'] : null;
                            $resultdata['videos'][$key]['description'] = !empty( $media['description'] ) ? $media['description'] : null;
                            $resultdata['videos'][$key]['views_total'] = !empty( $media['views_total'] ) ? $media['views_total'] : 0;
                            $resultdata['videos'][$key]['tags'] = !empty( $media['tags'] ) ? $media['tags'] : null;
                            $resultdata['videos'][$key]['channel.name'] = !empty( $media['channel.name'] ) ? $media['channel.name'] : null;
                            $resultdata['videos'][$key]['created_time'] = !empty( $media['created_time'] ) ? $media['created_time'] : null;
                            $resultdata['videos'][$key]['duration'] = !empty( $media['duration'] ) ? $media['duration'] : null;
                            $resultdata['videos'][$key]['owner.screenname'] = !empty( $media['owner.screenname'] ) ? $media['owner.screenname'] : null;
                            $resultdata['videos'][$key]['private'] = !empty( $media['private'] ) ? $media['private'] : null;
                        }
                        $resultdata['total_record'] = !empty( $result['total'] ) ? $result['total'] : null;
                        $resultdata['has_more'] = !empty( $result['has_more'] ) ? $result['has_more'] : null;
                    }
                    return ( $resultdata );
                }
                catch ( Exception $e ) {
                    watchdog('dailymotion', 'Function : getDailymotionVideoList all video Mesg : @msg', array('@msg' => $e->getMessage()) , WATCHDOG_ERROR);
                    return $resultdata['error'] = $e->getMessage();
                }
                break;
            case 'me':
                try {
                    if ( !empty( $search_title ) && $status != 'all') {
                        $result = $this->dailymotion->get( '/me/videos?sort=relevance', array(
                             'fields' => $fields,
                            'page' => ( $page_no ),
                            'limit' => ( $per_page ),
                            'search' => $search_title,
                            'private' => $status,
                        ) );
                    } else if( !empty( $search_title ) && $status == 'all') {
                        $result = $this->dailymotion->get( '/me/videos?sort=relevance', array(
                             'fields' => $fields,
                            'page' => ( $page_no ),
                            'search' => $search_title,
                            'limit' => ( $per_page )
                        ) );
                    } else if(empty( $search_title ) && $status == 'all') {
                        $result = $this->dailymotion->get( '/me/videos?filters=creative-official', array(
                             'fields' => $fields,
                            'page' => ( $page_no ),
                            'limit' => ( $per_page )
                        ) );
                    } else if ( empty( $search_title ) && $status != 'all') {
                        $result = $this->dailymotion->get( '/me/videos?filters=creative-official', array(
                             'fields' => $fields,
                            'page' => ( $page_no ),
                            'limit' => ( $per_page ),
                            'private' => $status
                        ) );
                    }
                    if ( isset( $result['list'] ) && !empty( $result['list'] ) ) {
                        foreach ( $result['list'] as $key => $media ) {
                            $resultdata['videos'][$key]['id'] = !empty( $media['id'] ) ? $media['id'] : null;
                            $resultdata['videos'][$key]['title'] = !empty( $media['title'] ) ? $media['title'] : null;
                            $resultdata['videos'][$key]['embed_url'] = !empty( $media['embed_url'] ) ? $media['embed_url'] : null;
                            $resultdata['videos'][$key]['thumbnail_url'] = !empty( $media['thumbnail_url'] ) ? $media['thumbnail_url'] : null;
                            $resultdata['videos'][$key]['description'] = !empty( $media['description'] ) ? $media['description'] : null;
                            $resultdata['videos'][$key]['views_total'] = !empty( $media['views_total'] ) ? $media['views_total'] : 0;
                            $resultdata['videos'][$key]['tags'] = !empty( $media['tags'] ) ? $media['tags'] : null;
                            $resultdata['videos'][$key]['channel.name'] = !empty( $media['channel.name'] ) ? $media['channel.name'] : null;
                            $resultdata['videos'][$key]['created_time'] = !empty( $media['created_time'] ) ? $media['created_time'] : null;
                            $resultdata['videos'][$key]['duration'] = !empty( $media['duration'] ) ? $media['duration'] : null;
                            $resultdata['videos'][$key]['owner.screenname'] = !empty( $media['owner.screenname'] ) ? $media['owner.screenname'] : null;
                            $resultdata['videos'][$key]['private'] = !empty( $media['private'] ) ? $media['private'] : null;
                            $resultdata['videos'][$key]['published'] = !empty( $media['published'] ) ? $media['published'] : null;
                        }
                        $resultdata['has_more'] = $result['has_more'];
                    }
                    return ( $resultdata );
                }
                catch ( Exception $e ) {
                    watchdog('dailymotion', 'Function : getDailymotionVideoList me video Mesg : @msg', array('@msg' => $e->getMessage()) , WATCHDOG_ERROR);
                    return $resultdata['error'] = $e->getMessage();
                }
                break;
            case 'commented':
               
                try {
                    if ( !empty( $search_title ) && $status != 'all') {
                        $result = $this->dailymotion->get( '/me/videos?sort=relevance', array(
                             'fields' => $fields,
                            'page' => ( $page_no ),
                            'limit' => ( $per_page ),
                            'search' => $search_title,
                            'sort' => 'commented',
                            'private' => $status,
                        ) );
                    } else if( !empty( $search_title ) && $status == 'all') {
                        $result = $this->dailymotion->get( '/me/videos?sort=relevance', array(
                             'fields' => $fields,
                            'page' => ( $page_no ),
                            'search' => $search_title,
                            'limit' => ( $per_page ),
                            'sort' => 'commented'
                        ) );
                    } else if(empty( $search_title ) && $status == 'all') {
                        $result = $this->dailymotion->get( '/me/videos?filters=creative-official', array(
                             'fields' => $fields,
                            'page' => ( $page_no ),
                            'limit' => ( $per_page ),
                            'sort' => 'commented'
                        ) );
                    } else if ( empty( $search_title ) && $status != 'all') {
                        $result = $this->dailymotion->get( '/me/videos?filters=creative-official', array(
                             'fields' => $fields,
                            'page' => ( $page_no ),
                            'limit' => ( $per_page ),
                            'private' => $status,
                            'sort' => 'commented'
                        ) );
                    }
                    if ( isset( $result['list'] ) && !empty( $result['list'] ) ) {
                        foreach ( $result['list'] as $key => $media ) {
                            $resultdata['videos'][$key]['id'] = !empty( $media['id'] ) ? $media['id'] : null;
                            $resultdata['videos'][$key]['title'] = !empty( $media['title'] ) ? $media['title'] : null;
                            $resultdata['videos'][$key]['embed_url'] = !empty( $media['embed_url'] ) ? $media['embed_url'] : null;
                            $resultdata['videos'][$key]['thumbnail_url'] = !empty( $media['thumbnail_url'] ) ? $media['thumbnail_url'] : null;
                            $resultdata['videos'][$key]['description'] = !empty( $media['description'] ) ? $media['description'] : null;
                            $resultdata['videos'][$key]['views_total'] = !empty( $media['views_total'] ) ? $media['views_total'] : 0;
                            $resultdata['videos'][$key]['tags'] = !empty( $media['tags'] ) ? $media['tags'] : null;
                            $resultdata['videos'][$key]['channel.name'] = !empty( $media['channel.name'] ) ? $media['channel.name'] : null;
                            $resultdata['videos'][$key]['created_time'] = !empty( $media['created_time'] ) ? $media['created_time'] : null;
                            $resultdata['videos'][$key]['duration'] = !empty( $media['duration'] ) ? $media['duration'] : null;
                            $resultdata['videos'][$key]['owner.screenname'] = !empty( $media['owner.screenname'] ) ? $media['owner.screenname'] : null;
                            $resultdata['videos'][$key]['private'] = !empty( $media['private'] ) ? $media['private'] : null;
                            $resultdata['videos'][$key]['published'] = !empty( $media['published'] ) ? $media['published'] : null;
                        }
                        $resultdata['has_more'] = $result['has_more'];
                    }
                    return ( $resultdata );
                }
                catch ( Exception $e ) {
                    watchdog('dailymotion', 'Function : getDailymotionVideoList comented video Mesg : @msg', array('@msg' => $e->getMessage()) , WATCHDOG_ERROR);
                    return $resultdata['error'] = $e->getMessage();
                }
                break;
            case 'rated':
                try {                    
                    if ( !empty( $search_title ) && $status != 'all') {
                        $result = $this->dailymotion->get( '/me/videos?sort=relevance', array(
                             'fields' => $fields,
                            'page' => ( $page_no ),
                            'limit' => ( $per_page ),
                            'search' => $search_title,
                            'sort' => 'rated',
                            'private' => $status,
                        ) );
                    } else if( !empty( $search_title ) && $status == 'all') {
                        $result = $this->dailymotion->get( '/me/videos?sort=relevance', array(
                             'fields' => $fields,
                            'page' => ( $page_no ),
                            'search' => $search_title,
                            'limit' => ( $per_page ),
                            'sort' => 'rated'
                        ) );
                    } else if(empty( $search_title ) && $status == 'all') {
                        $result = $this->dailymotion->get( '/me/videos?filters=creative-official', array(
                             'fields' => $fields,
                            'page' => ( $page_no ),
                            'limit' => ( $per_page ),
                            'sort' => 'rated'
                        ) );
                    } else if ( empty( $search_title ) && $status != 'all') {
                        $result = $this->dailymotion->get( '/me/videos?filters=creative-official', array(
                             'fields' => $fields,
                            'page' => ( $page_no ),
                            'limit' => ( $per_page ),
                            'private' => $status,
                            'sort' => 'rated'
                        ) );
                    }
                    if ( isset( $result['list'] ) && !empty( $result['list'] ) ) {
                        foreach ( $result['list'] as $key => $media ) {
                            $resultdata['videos'][$key]['id'] = !empty( $media['id'] ) ? $media['id'] : null;
                            $resultdata['videos'][$key]['title'] = !empty( $media['title'] ) ? $media['title'] : null;
                            $resultdata['videos'][$key]['embed_url'] = !empty( $media['embed_url'] ) ? $media['embed_url'] : null;
                            $resultdata['videos'][$key]['thumbnail_url'] = !empty( $media['thumbnail_url'] ) ? $media['thumbnail_url'] : null;
                            $resultdata['videos'][$key]['description'] = !empty( $media['description'] ) ? $media['description'] : null;
                            $resultdata['videos'][$key]['views_total'] = !empty( $media['views_total'] ) ? $media['views_total'] : 0;
                            $resultdata['videos'][$key]['tags'] = !empty( $media['tags'] ) ? $media['tags'] : null;
                            $resultdata['videos'][$key]['channel.name'] = !empty( $media['channel.name'] ) ? $media['channel.name'] : null;
                            $resultdata['videos'][$key]['created_time'] = !empty( $media['created_time'] ) ? $media['created_time'] : null;
                            $resultdata['videos'][$key]['duration'] = !empty( $media['duration'] ) ? $media['duration'] : null;
                            $resultdata['videos'][$key]['owner.screenname'] = !empty( $media['owner.screenname'] ) ? $media['owner.screenname'] : null;
                            $resultdata['videos'][$key]['private'] = !empty( $media['private'] ) ? $media['private'] : null;
                            $resultdata['videos'][$key]['published'] = !empty( $media['published'] ) ? $media['published'] : null;
                        }
                        $resultdata['has_more'] = $result['has_more'];
                    }
                    return ( $resultdata );
                }
                catch ( Exception $e ) {
                    watchdog('dailymotion', 'Function : getDailymotionVideoList rated video Mesg : @msg', array('@msg' => $e->getMessage()) , WATCHDOG_ERROR);
                    return $resultdata['error'] = $e->getMessage();
                }
                break;
            case 'visited':
                try {
                    if ( !empty( $search_title ) && $status != 'all') {
                        $result = $this->dailymotion->get( '/me/videos?sort=relevance', array(
                             'fields' => $fields,
                            'page' => ( $page_no ),
                            'limit' => ( $per_page ),
                            'search' => $search_title,
                            'sort' => 'visited',
                            'private' => $status,
                        ) );
                    } else if( !empty( $search_title ) && $status == 'all') {
                        $result = $this->dailymotion->get( '/me/videos?sort=relevance', array(
                             'fields' => $fields,
                            'page' => ( $page_no ),
                            'search' => $search_title,
                            'limit' => ( $per_page ),
                            'sort' => 'visited'
                        ) );
                    } else if(empty( $search_title ) && $status == 'all') {
                        $result = $this->dailymotion->get( '/me/videos?filters=creative-official', array(
                             'fields' => $fields,
                            'page' => ( $page_no ),
                            'limit' => ( $per_page ),
                            'sort' => 'visited'
                        ) );
                    } else if ( empty( $search_title ) && $status != 'all') {
                        $result = $this->dailymotion->get( '/me/videos?filters=creative-official', array(
                             'fields' => $fields,
                            'page' => ( $page_no ),
                            'limit' => ( $per_page ),
                            'private' => $status,
                            'sort' => 'visited'
                        ) );
                    }
                    if ( isset( $result['list'] ) && !empty( $result['list'] ) ) {
                        foreach ( $result['list'] as $key => $media ) {
                            $resultdata['videos'][$key]['id'] = !empty( $media['id'] ) ? $media['id'] : null;
                            $resultdata['videos'][$key]['title'] = !empty( $media['title'] ) ? $media['title'] : null;
                            $resultdata['videos'][$key]['embed_url'] = !empty( $media['embed_url'] ) ? $media['embed_url'] : null;
                            $resultdata['videos'][$key]['thumbnail_url'] = !empty( $media['thumbnail_url'] ) ? $media['thumbnail_url'] : null;
                            $resultdata['videos'][$key]['description'] = !empty( $media['description'] ) ? $media['description'] : null;
                            $resultdata['videos'][$key]['views_total'] = !empty( $media['views_total'] ) ? $media['views_total'] : 0;
                            $resultdata['videos'][$key]['tags'] = !empty( $media['tags'] ) ? $media['tags'] : null;
                            $resultdata['videos'][$key]['channel.name'] = !empty( $media['channel.name'] ) ? $media['channel.name'] : null;
                            $resultdata['videos'][$key]['created_time'] = !empty( $media['created_time'] ) ? $media['created_time'] : null;
                            $resultdata['videos'][$key]['duration'] = !empty( $media['duration'] ) ? $media['duration'] : null;
                            $resultdata['videos'][$key]['owner.screenname'] = !empty( $media['owner.screenname'] ) ? $media['owner.screenname'] : null;
                            $resultdata['videos'][$key]['private'] = !empty( $media['private'] ) ? $media['private'] : null;
                            $resultdata['videos'][$key]['published'] = !empty( $media['published'] ) ? $media['published'] : null;
                        }
                        $resultdata['has_more'] = $result['has_more'];
                    }
                    return ( $resultdata );
                }
                catch ( Exception $e ) {
                    watchdog('dailymotion', 'Function : getDailymotionVideoList visited video Mesg : @msg', array('@msg' => $e->getMessage()) , WATCHDOG_ERROR);
                    return $resultdata['error'] = $e->getMessage();
                }
                break;
        }
    }
    
     /**
     * Get dailymotion video lists
     */
    public function getDMSideVideos( $flag = NULL, $fields = array( 'id', 'title', 'embed_url', 'thumbnail_url', 'private', 'type' ), $page_no = 1, $per_page = 10, $search_title )
    {
        $resultdata = array();
        switch ( $flag ) {
            case 'me':
                try {
                    if ( !empty( $search_title ) ) {
                        $result = $this->dailymotion->get( '/me/videos', array(
                             'fields' => $fields,
                            'page' => ( $page_no ),
                            'limit' => ( $per_page ),
                            'search' => $search_title
                        ) );
                    } else {
                        $result = $this->dailymotion->get( '/me/videos?filters=creative-official', array(
                             'fields' => $fields,
                            'page' => ( $page_no ),
                            'limit' => ( $per_page )
                        ) );
                    }
                    if ( isset( $result['list'] ) && !empty( $result['list'] ) ) {
                        foreach ( $result['list'] as $key => $media ) {
                            $resultdata['videos'][$key]['id'] = !empty( $media['id'] ) ? $media['id'] : null;
                            $resultdata['videos'][$key]['title'] = !empty( $media['title'] ) ? $media['title'] : null;
                            $resultdata['videos'][$key]['embed_url'] = !empty( $media['embed_url'] ) ? $media['embed_url'] : null;
                            $resultdata['videos'][$key]['thumbnail_url'] = !empty( $media['thumbnail_url'] ) ? $media['thumbnail_url'] : null;
                            $resultdata['videos'][$key]['description'] = !empty( $media['description'] ) ? $media['description'] : null;
                            $resultdata['videos'][$key]['views_total'] = !empty( $media['views_total'] ) ? $media['views_total'] : 0;
                            $resultdata['videos'][$key]['duration'] = !empty( $media['duration'] ) ? $media['duration'] : null;
                            $resultdata['videos'][$key]['owner.screenname'] = !empty( $media['owner.screenname'] ) ? $media['owner.screenname'] : null;
                        }
                        $resultdata['has_more'] = $result['has_more'];
                    }
                    return ( $resultdata );
                }
                catch ( Exception $e ) {
                    watchdog('dailymotion', 'Function : getDailymotionVideoList me video Mesg : @msg', array('@msg' => $e->getMessage()) , WATCHDOG_ERROR);
                    return $resultdata['error'] = $e->getMessage();
                }
                break;
        }
    }
    
    /**
     * Delete dailymotion video by its video id
     */
    public function deleteDailyMotionVideo( $video_id = null )
    {
        try {
            $result = $this->dailymotion->delete( "/video/$video_id" );
            watchdog('dailymotion', 'Dailymotion video deleted successfully by @user and video ID  @video_id', array('@user' => @$GLOBALS['user']->name, '@video_id' => $video_id), WATCHDOG_INFO);
        }
        catch ( Exception $e ) {
            watchdog('dailymotion', 'Function : deleteDailyMotionVideo Mesg : @msg', array('@msg' => $e->getMessage()) , WATCHDOG_ERROR);
            return $resultdata['error'] = $e->getMessage();
        }
    }
    
    /**
     * Get dailymotion video details
     */
    public function getDailyMotionVideoDetail( $videoId = null, $fields = array( 'id', 'title', 'embed_url', 'channel', 'thumbnail_url', 'description', 'tags', 'private', 'type' ) )
    {
        $result = $result_error = array();
        try {
            
            $result = $this->dailymotion->get( "/video/$videoId", array(
                 'fields' => $fields 
            ) );
            return $result;
        }
        catch ( Exception $e ) {
            watchdog('dailymotion', 'Function : getDailyMotionVideoDetail Mesg : @msg', array('@msg' => $e->getMessage()) , WATCHDOG_ERROR);
            return $result_error['error'] = $e->getMessage();
        }
    }
    
    /**
     * Update dailymotion video data
     */
    public function visitorCountry( )
    {
        $result = "";
        if ( @filter_var( $_SERVER['HTTP_CLIENT_IP'], FILTER_VALIDATE_IP ) ) {
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        } elseif ( @filter_var( $_SERVER['HTTP_X_FORWARDED_FOR'], FILTER_VALIDATE_IP ) ) {
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            $ip = $_SERVER['REMOTE_ADDR'];
        }
        $ip_data = @json_decode( file_get_contents( "http://www.geoplugin.net/json.gp?ip=" . $ip ) );
        if ( $ip_data && $ip_data->geoplugin_countryCode != null ) {
            $result = $ip_data->geoplugin_countryCode;
        }
        return !empty( $result ) ? $result : 'us';
    }
    
    /**
     * Update dailymotion video data
     */
    public function updateDailymotionVideoData( $videoId = null, $data = array( ) )
    {
        try {
            $result = $this->dailymotion->post( "/video/$videoId", $data );
            watchdog('dailymotion', 'Dailymotion video data updated successfully by @user and video ID  @video_id', array('@user' => @$GLOBALS['user']->name, '@video_id' => $videoId), WATCHDOG_INFO);
        }
        catch ( Exception $e ) {
            watchdog('dailymotion', 'Function : updateDailymotionVideoData Mesg : @msg', array('@msg' => $e->getMessage()) , WATCHDOG_ERROR);
            return $resultdata['error'] = $e->getMessage();
        }
    }
    
    /**
     * Update dailymotion video data
     */
    public function getDailymotionConnectedInformation( )
    {
        $resultdata = array();
        $videoresult = $this->dailymotion->get( '/me/videos', array(
             'fields' => array(
                 'id',
                'title',
                'created_time' 
            ),
            'page' => 1,
            'limit' => 10 
        ) );
        $userresult = $this->dailymotion->get( '/user/me', array(
             'fields' => array(
                 'id',
                'screenname',
                'type',
                'avatar_120_url' 
            ) 
        ) );
        $resultdata['screenname'] = !empty( $userresult['screenname'] ) ? $userresult['screenname'] : 'screenname not found';
        $resultdata['total_record'] = !empty( $videoresult['total'] ) ? $videoresult['total'] : 0;
        $resultdata['user_photo'] = !empty( $userresult['avatar_120_url'] ) ? $userresult['avatar_120_url'] : '';
        $resultdata['last_uploaded'] = !empty( $videoresult['list'][0]['created_time'] ) ? date( 'M d, Y', $videoresult['list'][0]['created_time'] ) : 'Not Found';
        return $resultdata;
    }
    
    /**
     * Check user is official
     */
    public function getUserType( )
    {
        $userresult = $this->dailymotion->get( '/user/me', array(
             'fields' => array(
                 'type' 
            ) 
        ) );
        if ( !empty( $userresult['type'] ) && ( $userresult['type'] == 'official' ) ) {
            return true;
        } else {
            return false;
        }
    }
    
    /**
     * Update dailymotion video data
     */
    public function getDMAuthorizationUrl( $display = 'popup' )
    {
        return $this->dailymotion->getAuthorizationUrl( $display );
    }
    
    /**
     * Upload dailymotion video
     */
    public function uploadVideoOnDailymotion( $testVideoFile = null, $video_title = 'Dailymotion Video', $channel = null )
    {
        $url = $this->dailymotion->uploadFile( $testVideoFile );
        if ( !empty( $channel ) ) {
            $media = $this->dailymotion->post( '/videos', array(
                 'url' => $url,
                'title' => $video_title,
                'published' => true,
                'channel' => $channel 
            ) );
        } else {
            $media = $this->dailymotion->post( '/videos', array(
                 'url' => $url,
                'title' => $video_title,
                'published' => true 
            ) );
        }
        watchdog('dailymotion', 'Dailymotion video upload successfully by @user and video ID  @video_id', array('@user' => @$GLOBALS['user']->name, '@video_id' => $media['id']), WATCHDOG_INFO);
        return $media;
    }
}