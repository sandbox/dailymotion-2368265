<?php
/*
* Method to render Dailymotion Cloud video gallery
*/
function dmc_gallery_callback(){
        $page         = (isset($_GET['page'])) ? $_GET['page'] : 0;
        $pageno       = $page + 1;
        $itemsPerPage = 6;
        $output = '';
        $sortby = isset($_GET['filter']) && !empty($_GET['filter']) ? $_GET['filter'] : '-created';
        $search_title = (isset($_REQUEST['dmc_video_title']) && !empty($_REQUEST['dmc_video_title']))?$_REQUEST['dmc_video_title']:'';
        if (!empty($GLOBALS['CLOUD_CON_INFO']['DMC_USER_ID']) && !empty($GLOBALS['CLOUD_CON_INFO']['DMC_API_KEY'])) {
            $dmCloud = new DailymotionCloudOwnMethod($GLOBALS['CLOUD_CON_INFO']['DMC_USER_ID'], $GLOBALS['CLOUD_CON_INFO']['DMC_API_KEY']);
            $output  = (array) $dmCloud->getDailymotionCloudVideos((int) $pageno, $itemsPerPage, $search_title, $sortby);
        }
        // Call theme() function, so that Drupal includes the custom-page.tpl.php template
        return theme('dmc_gallery_template', array('result' => $output,'selected' => $sortby,'search_title' => $search_title,'pageno' => $pageno));
}
/*
* Method to generate filter for Dailymotion cloud videos
*/
function dmc_filter_form($form, &$form_state)
{
    $search_title = (isset($_REQUEST['dmc_video_title']) && !empty($_REQUEST['dmc_video_title']))?$_REQUEST['dmc_video_title'] : '';
    $sortby = isset($_GET['filter']) && !empty($_GET['filter']) ? $_GET['filter'] : '-created';
    
    $form['#method'] = 'get';
    
    $form['dmc_video_title'] = array(
        '#type' => 'textfield',
        '#default_value' => $search_title,
        '#required' => TRUE,
        '#attributes' => array('id' => array('dmc-video-title'), 'placeholder'=> t('Search my videos')),
    );
    
    $form['search'] = array(
        '#type' => 'button',
        '#value' => t('Search'),
        '#attributes' => array('onclick' => 'this.form.submit();'),
    );
    
    $form['filter'] = array(
        '#type' => 'select',
        '#title' => t('Sort by'),
        '#default_value' => $sortby,
        '#options' => array('-created'=>'Most Recent','meta.title'=>'A-Z','-meta.title'=>'Z-A'),
        '#attributes' => array('onchange' => 'this.form.submit();', 'id' => array('dsl-filter')),
        '#prefix' => '<div class="dsl-sort">',
        '#suffix' => '</div>',
    );
    
    return $form;
}
/*
* Method to delete Dailymotion cloud video
*/
function delete_dmc_video($media_id)
{
    if (!empty($media_id)) {
        $dailymotioncloud = new DailymotionCloudOwnMethod($GLOBALS['CLOUD_CON_INFO']['DMC_USER_ID'], $GLOBALS['CLOUD_CON_INFO']['DMC_API_KEY']);
        $delete           = ($dailymotioncloud->deleteDailymotionCloudMedia($media_id)) ? true : false;
        drupal_set_message(t('Your video has been deleted.'));
        drupal_goto('admin/dm/video-gallery');
    }
}

/**
     * Method to edit Dailymotion cloud video by media Id
     *
     */
function edit_dmc_video_callback()
{
    global $base_url;
    $moduleURL = DAILYMOTION_FULL_URL;
    $media_id = (isset($_POST['mediaId'])) ? $_POST['mediaId'] : null;
    if (!empty($media_id)) {
        $dailymotioncloud = new DailymotionCloudOwnMethod($GLOBALS['CLOUD_CON_INFO']['DMC_USER_ID'], $GLOBALS['CLOUD_CON_INFO']['DMC_API_KEY']);
        $playerList = $dailymotioncloud->getDailymotionCloudPlayer();
        $videoInfo        = $dailymotioncloud->getDailymotionCloudVideosDetails($media_id);
        $mediaImageURL    = !empty($videoInfo['stream_url']) ? $videoInfo['stream_url'] : $moduleURL . '/img/no_files_found.jpg';
        $curpage          = !empty($_POST['curpage']) ? $_POST['curpage'] : 'notfound';
        $str              = '';
        $str .= '<div class="dm-edit-main dm_cloud dm-common">';
        $str .= '<div class="dmc-edit-container">';
        $str .= '<div class="logo">
                    <h2>'.t('Edit Video').'</h2>
                    <span class="logo"></span>
                </div>';
        $str .= '<script type="text/javascript" src="' . $moduleURL . '/js/ajax-upload_pattern.js"></script>';
        $str .= '<form enctype="multipart/form-data" action="" id="dm_update_form" method="post">';
        $str .= '<input type="hidden" id="counter-value" value="1" />';
        $str .= '<input type="hidden" id="curpage" name="curpage" value="'.$curpage.'" />';
        $str .= '<input type="hidden" name="media_id" size="50" value="' . $videoInfo['media_id'] . '" />';
        $str .= '<div class="top-row">
                    <div class="label">'.t('Thumbnail').':</div>
                    <div class="thumbnail">
                   <div class="thumb-img"><img class="edit-video-thumbnail" src="' . $mediaImageURL . '" alt="" /></div>
                   <div class="thumb-right">
                      <a id="browse_file" href="#">Change Thumbnail</a>
                      <input type="hidden" id="attach_id" name="at_id" value="" />
                      <input type="hidden" id="attach_url" name="attach_url" value="" />
                      <div class="msg">
                         <p>'.t('Minimum 150 px wide').'</p>
                         <p>'.t('Recommended aspect ratio: 4:3 or 16:9').'</p>
                      </div>
                   </div>
                    </div>
                 </div>';
        $str .= '<div class="middle-row">';
        $str .= '<div class="head">Custom Tags</div>';
        $str .= '<div class="middle-wrapper">';
        $str .= '<div class="title"><label>'.t('Video Title').':</label><input type="text" name="title" id="video-title" value="' . $videoInfo['meta']['title'] . '" /></div>';
        $str .= '<div class="present-tags">';
        if (!empty($videoInfo['meta'])) {
            $metatags = $videoInfo['meta'];
            $i        = 1;
            foreach ($metatags as $key => $val) {
                if ($key != 'title') {
                    $str .= '<div class="tag" id="meta_' . $key . '">
                <label>' . $key . '</label>
                <input type="hidden" size="50" class="keyInput" name="originalmeta[' . $key . '][]" value="' . $key . '" />
                <input type="text" name="originalmeta[' . $key . '][]" value="' . $val . '" />
                '.l(t("Remove"),'javascript:void(0)',array('attributes'=>array('class'=>'delete-tag', 'onclick'=>'deleteMetatags(\'' . $videoInfo['media_id'] . '\',\'' . $key . '\');'), 'fragment' => '','external'=>true)).'
                 </div>';
                }
                $i++;
            }
        }
        $str .= '</div>';
        $str .= '<div class="new-tags">';
        $str .= '<div class="tag"><input type="text" size="50" class="keyInput" name="meta[val1][]" placeholder="Name"><input size="50" type="text" name="meta[val1][]" placeholder="Value">'.l("Add",'javascript:void(0)',array('attributes'=>array('id'=>'dmc-new-tag'), 'fragment' => '','external'=>true)).'';
        $str .= '</div>';
        $str .= '</div>';
        $str .= '</div>';
        $str .= '</div>';
        $str .= '<div class="bottom-row">';
        $str .= '<div class="preview">
            <label>'.t('Preview').'</label>
            <div class="iframe"><iframe width="338" height="150" frameborder="0" scrolling="no" src="' . $videoInfo['embed_url'] . '"></iframe></div>
             </div>';
        $str .= '</div>';
        $str .= '<div class="alert-msg" id="dmc-message"></div>';
        $str .= '<div class="footer-row">
                <div class="delete">
                   '.l(t("Delete this video"),'javascript:void(0)',array('attributes'=>array('class'=>'dmc-trash-trigger'), 'fragment' => '','external'=>true)).'
                   <div class="confirm-box">
                      <div class="head"><span class="arrow"></span>'.t('Delete this video?').'</div>
                      <div class="message">'.t('This video will be deleted from your Dailymotion Cloud account.').'</div>
                      '.l(t("No, keep it"),'javascript:void(0)',array('attributes'=>array('class'=>'dmc-keep-it'), 'fragment' => '','external'=>true)).'
                      '.l(t("Yes, delete"), $base_url . '/admin/dmc/delete/' . $videoInfo['media_id'], array('attributes'=>array('class'=> 'dmc-delete-it'))).'
                   </div>
                </div>
                <div class="save">
                   '.l(t("Save"),'javascript:void(0)',array('attributes'=>array('class'=>'save_new_data', 'onclick' => 'getDMCupdatedvalues();'), 'fragment' => '','external'=>true)).'
                </div>
                </div>';
        $str .= '</form>';
        $str .= '</div>';
        $str .= '</div>';
        print json_encode($str); exit;
    }
}

/**
     * Method calling from ajax to delete the Dailymotion cloud video meta tags
     *
     */
function delete_dmc_metatags()
{
    $mediaId = (isset($_POST['mediaId'])) ? $_POST['mediaId'] : null;
    $key     = (isset($_POST['key'])) ? $_POST['key'] : null;
    if (!empty($mediaId) && !empty($key)) {
        $dailymotioncloud = new DailymotionCloudOwnMethod($GLOBALS['CLOUD_CON_INFO']['DMC_USER_ID'], $GLOBALS['CLOUD_CON_INFO']['DMC_API_KEY']);
        $dailymotioncloud->removeDailymotionCloudVideoMetas($mediaId, array(
            $key
        ));
    }
}

/**
* Method to update Dailymotion cloud Meta tags
*
*/
function update_dmc_video()
{
   $meta             = array();
   $originalmetameta = array();
   $mediaId          = !empty($_POST['media_id']) ? $_POST['media_id'] : null;
   $imageurl         = !empty($_POST['attach_url']) ? $_POST['attach_url'] : null;
   $imageid          = !empty($_POST['at_id']) ? $_POST['at_id'] : null;
   if ($mediaId) {
       $dailymotioncloud = new DailymotionCloudOwnMethod($GLOBALS['CLOUD_CON_INFO']['DMC_USER_ID'], $GLOBALS['CLOUD_CON_INFO']['DMC_API_KEY']);
       $meta['title']    = !empty($_POST['title']) ? $_POST['title'] : null;
       $updatemeta       = !empty($_POST['originalmeta']) ? array_keys($_POST['originalmeta']) : null;
       if (!empty($_POST['meta'])) {
           foreach ($_POST['meta'] as $key => $data) {
               if (!empty($data[0]) && !empty($data[1])) {
                   $meta[$data[0]] = (string) $data[1];
               }
           }
       }
       if (!empty($_POST['originalmeta'])) {
           foreach ($_POST['originalmeta'] as $originalmetakey => $originalmetadata) {
               if (!empty($originalmetadata[0]) && !empty($originalmetadata[1])) {
                   $originalmetameta[$originalmetadata[0]] = (string) $originalmetadata[1];
               }
           }
       }
       if ($imageurl) {
           $dailymotioncloud->setDailyMotionCloudVideoThumbnail($mediaId, $imageurl);
       }
       if ($updatemeta) {
           $dailymotioncloud->removeDailymotionCloudVideoMetas($mediaId, $updatemeta);
           $dailymotioncloud->setDailyMotionCloudVideoMetas($mediaId, $originalmetameta);
       }
       if ($meta) {
           $dailymotioncloud->setDailyMotionCloudVideoMetas($mediaId, $meta);
       }
       drupal_set_message(t('Your video was saved successfully.'));
       print json_encode(array(
           'msg' => 'Succesfuly update data'
       ));
       exit;
   }
}
/*
* Method to get DMC Videos
*/
function get_dmc_video_callback()
{
    $search_title = (isset($_POST['title']) && !empty($_POST['title']))?$_POST['title']:'';
    $pageno = (isset($_POST['pageno']) && !empty($_POST['pageno']))?$_POST['pageno']:1;
    $itemsPerPage = 5;
    $dmCloud = new DailymotionCloudOwnMethod($GLOBALS['CLOUD_CON_INFO']['DMC_USER_ID'], $GLOBALS['CLOUD_CON_INFO']['DMC_API_KEY']);
    $output  = (array) $dmCloud->getDailymotionCloudVideos((int) $pageno, $itemsPerPage, $search_title);
    print json_encode($output);exit;
}
