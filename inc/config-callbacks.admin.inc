<?php


/**
 * Implements Dailymotion configuration setting form.
 */
function dailymotion_configuration_form($form, &$form_state)
{
    $form = array();
    
    $form['dm_api_key'] = array(
        '#title' => t('Dailymotion Api Key'),
        '#type' => 'textfield',
        '#required' => true,
        '#default_value' => variable_get('dm_api_key', '')
    );
    
    $form['dm_api_secret'] = array(
        '#title' => t('Dailymotion App secret'),
        '#type' => 'textfield',
        '#required' => true,
        '#default_value' => variable_get('dm_api_secret', '')
    );
    
    $form['dm_username'] = array(
        '#title' => t('Dailymotion Username'),
        '#type' => 'textfield',
        '#required' => true,
        '#default_value' => variable_get('dm_username', '')
    );
    
    $form['dm_password'] = array(
        '#title' => t('Dailymotion Password'),
        '#type' => 'textfield',
        '#required' => true,
        '#default_value' => variable_get('dm_password', '')
    );
    
    $form['dm_test'] = array(
        '#markup' => l(t('Join Dailymotion Publisher Program'), 'http://publisher.dailymotion.com/', array(
            'attributes' => array(
                'target' => '_blank'
            )
        ))
    );
    
    return system_settings_form($form);
}

/**
 * Implements Dailymotion cloud configuration setting form.
 */
function dailymotion_cloud_configuration_form($form, &$form_state)
{
    $form = array();
    
    $form['dmc_user_id'] = array(
        '#title' => t('Dailymotion Cloud userID'),
        '#type' => 'textfield',
        '#required' => true,
        '#default_value' => variable_get('dmc_user_id', '')
    );
    
    $form['dmc_api_key'] = array(
        '#title' => t('Dailymotion Cloud Api Key'),
        '#type' => 'textfield',
        '#required' => true,
        '#default_value' => variable_get('dmc_api_key', '')
    );
    
    $form['dm_test'] = array(
        '#markup' => t("<h3>Don't have a Dailymotion Cloud account?</h3>") . l(t('Open a free trail'), 'https://www.dmcloud.net/register/', array(
            'attributes' => array(
                'target' => '_blank'
            )
        ))
    );
    
    return system_settings_form($form);
}
