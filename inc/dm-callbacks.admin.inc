<?php

/**
     * Method to render Dailymotion video gallery
     */
function dm_gallery_callback()
{
    $pageno = (isset($_REQUEST['pageno']))?$_REQUEST['pageno']:1;
    $itemsPerPage = 6;
    $string      = '';
    $selected = isset($_REQUEST['filter']) && !empty($_REQUEST['filter']) ? $_REQUEST['filter'] : 'me';
    $search_title = (isset($_REQUEST['dm_video_title']) && !empty($_REQUEST['dm_video_title']))?$_REQUEST['dm_video_title'] : '';
    $status = isset($_REQUEST['status']) || !empty($_REQUEST['status']) ? $_REQUEST['status'] : 'all';
    $dailymotion = new DailymotionOwnMethod();
    $output      = $dailymotion->getDailymotionVideoList($selected, $fields = array(
        'id',
        'title',
        'embed_url',
        'thumbnail_url',
        'created_time',
        'views_total',
        'tags',
        'channel.name',
        'duration',
        'description',
        'private',
        'published'
    ), $status, (int)$pageno, $itemsPerPage, $search_title);
     // Call theme() function, so that Drupal includes the custom-page.tpl.php template
    return theme('dm_gallery_template', array('result' => $output,'selected' => $selected,'search_title' => $search_title,'pageno' => $pageno, 'status'=>$status));
    
}

/**
     * Method to generate filter form for Dailymotion videos gallery
     */
function dm_filter_form($form, &$form_state)
{
    $search_title = (isset($_REQUEST['dm_video_title']) && !empty($_REQUEST['dm_video_title']))?$_REQUEST['dm_video_title'] : '';
    $selected = isset($_REQUEST['filter']) && !empty($_REQUEST['filter']) ? $_REQUEST['filter'] : 'me';
    $status = (isset($_REQUEST['status']) || !empty($_REQUEST['status'])) ? $_REQUEST['status'] : 'all';
    
    $form['#method'] = 'get';
    
    $form['dm_video_title'] = array(
        '#type' => 'textfield',
        '#default_value' => $search_title,
        '#required' => TRUE,
        '#attributes' => array('id' => array('dm-video-title'), 'placeholder'=> t('Search my videos')),
    );
    
    $form['search'] = array(
        '#type' => 'button',
        '#value' => t('Search'),
        '#attributes' => array('onclick' => 'this.form.submit();'),
    );
    
    $form['status'] = array(
        '#type' => 'select',
        '#default_value' => $status,
        '#options' => array('all'=>'All','0'=>'Public only','1'=>'Private only'),
        '#attributes' => array('onchange' => 'this.form.submit();', 'class' => array('private-public')),
        '#prefix' => '<div class="dsl-filter">',
        '#suffix' => '</div>',
    );
    
    $form['filter'] = array(
        '#type' => 'select',
        '#title' => t('Sort by'),
        '#default_value' => $selected,
        '#options' => array('me'=>'Most Recent','commented'=>'Most Commented','rated'=>'Most Liked','visited'=>'Most Viewed'),
        '#attributes' => array('onchange' => 'this.form.submit();', 'id' => array('dsl-filter')),
        '#prefix' => '<div class="dsl-sort">',
        '#suffix' => '</div>',
    );
    
    return $form;
}

/**
     * Method to delete Dailymotion video
     */
function delete_dm_video($videoId)
{
    if (isset($videoId)) {
         $dailymotion = new DailymotionOwnMethod();
         $dailymotion->deleteDailyMotionVideo($videoId);
         drupal_set_message(t('Your video has been deleted.'));
         drupal_goto('admin/dm/video-gallery/dailymotion');
    }
}

/**
     * Method to edit Dailymotion video by media Id
     */
function edit_dm_video_callback()
{
    global $base_url;
    $moduleURL = $base_url . '/'.drupal_get_path('module', 'dailymotion');
    $media_id = (isset($_POST['mediaId'])) ? $_POST['mediaId'] : null;
    if (!empty($media_id)) {
        $dailymotion   = new DailymotionOwnMethod();
        $videoInfo     = $dailymotion->getDailyMotionVideoDetail($media_id);
        $chennelslist  = $dailymotion->getDailymotionChannelList();
        $mediaImageURL = !empty($videoInfo['thumbnail_url']) ? $videoInfo['thumbnail_url'] : $moduleURL . '/img/no_files_found.jpg';
        $description   = !empty($videoInfo['description']) ? $videoInfo['description'] : '';
        $tags          = !empty($videoInfo['tags']) ? implode(', ', $videoInfo['tags']) : '';
        $Channels      = !empty($videoInfo['channel']) ? $videoInfo['channel'] : '';
        $str           = '';
        $str .= '<script type="text/javascript" src="' . $moduleURL . '/js/ajax-upload_pattern.js"></script>';
        $str .= '<div class="dm-edit-main dm-common">';
        $str .= '<div class="dmc-edit-container">';
        $str .= '<form enctype="multipart/form-data" action="" id="dm_update_form" method="post">';
        $str .= '<input type="hidden" name="id" size="50" value="' . $media_id . '" />';
        $str .= '<div class="logo">
                    <h2>'.t('Edit Video').'</h2>
                    <span class="logo"></span>
                </div>';
        $str .= '<div class="title-wrap">
                    <label><span class="required">*</span>'.t('Video Title').':</label>
                    <input type="text" name="data[title]" id="video-title" value="' . $videoInfo['title'] . '" />
                </div>';
        $str .= '<div class="desc-wrap">
                    <label>'.t('Video Description').':</label>
                    <textarea  name="data[description]" id="video-title">' . $description . '</textarea>
                 </div>';
        $str .= '<div class="channel-wrap">
                 <label><span class="required">*</span>Channel: <span class="qus_mark tooltip"><span><img class="callout" src="'.$moduleURL . '/img/callout.gif" />Dailymotion Publisher allows you to earn advertising revenue when sharing Dailymotion videos on your site.</span></span></label>
                 <select type="text" id="channel" name="data[channel]">';
        $str .= '<option value="">'.t('Please select').'</option>';
        if (isset($chennelslist) && !empty($chennelslist)) {
            foreach ($chennelslist as $ck => $cv) {
                (isset($Channels) && ($Channels == $ck)) ? $selected = 'selected="selected"' : $selected = '';
                $str .= '<option value="' . $ck . '" ' . $selected . '>' . $cv . '</option>';
            }
        }
        $str .= '</select></div>';
        $change = '';
        if (isset($videoInfo['type']) && $videoInfo['type'] == 'official'):
            $change = '<a id="browse_file" href="#">Change Thumbnail</a>
                 <input type="hidden" id="attach_id" name="at_id" value="" />
                 <input type="hidden" id="attach_url" name="attach_url" value="" />';
        endif;
        $str .= '<div class="thumb-wrap">
                 <label>'.t('Thumbnail').':</label>
                 <div class="video-thumb">
                    <img class="edit-video-thumbnail" src="' . $mediaImageURL . '" alt="" width="150" height="150"/>
                 </div>
                 <div class="thumb-right">
                    ' . $change . '
                    <div class="msg">
                       <p>'.t('Minimum 150 px wide').'</p>
                       <p>'.t('Recommended aspect ratio: 4:3 or 16:9').'</p>
                    </div>
                 </div>
              </div>';
        $str .= '<div class="tags-wrap">
                    <label>'.t('Tag(s)').':</label>
                    <input type="text" class="tags" name="data[tags]" maxlength="250" size="50" id="video-tags" value="' . $tags . '" />
                </div>';
        $checked_private = (!empty($videoInfo['private']) && $videoInfo['private'] == 1) ? 'checked="checked"' : '';
        $checked_public  = (!empty($videoInfo['private']) && $videoInfo['private'] == 1) ? '' : 'checked="checked"';
        
        $class_private = (isset($videoInfo['private']) && $videoInfo['private'] == 1) ? 'blur' : '';
        $class_public  = (isset($videoInfo['private']) && $videoInfo['private'] == 1) ? '' : 'blur';
        
        $str .= '<div class="visibility-wrap visibility_private '.$class_public.'">
                    <label>'.t('Visibility').':</label>
                    <input id="dm-video-private" type="radio" name="dm_video_status" value="1" name="data[private]" ' . $checked_private . '>Private
                    <div class="cls_visibility">
                       <p>'.t('This video is private and accessable through private link below').'</p>
                       <span>'.t('Private URL').'</soan>
                    </div>
              </div>';
        $str .= '<div class="visibility-wrap visibility_public '.$class_private.'">
                    <input id="dm-video-public" type="radio" name="dm_video_status" value="0" name="data[private]" ' . $checked_public . '>Public
                    <div class="cls_visibility">
                       <p>'.t('This video is public and can be seen by anyone on Dailymotion.com.').'</p>
                       <span>'.t('Private URL').'</span>
                    </div>
              </div>';
        $str .= '<div class="alert-msg" id="dm-message"></div>';
        $str .= '<div class="footer-row">
                            <div class="delete">
                                '.l("Delete this video",'javascript:void(0)',array('attributes'=>array('class'=>'dmc-trash-trigger'), 'fragment' => '','external'=>true)).'
                               <div class="confirm-box">
                                  <div class="head"><span class="arrow"></span>'.t('Delete this video?').'</div>
                                  <div class="message">'.t('This video will be deleted from your Dailymotion.com account.').'</div>
                                  '.l(t("No, keep it"),'javascript:void(0)',array('attributes'=>array('class'=>'dmc-keep-it'), 'fragment' => '','external'=>true)).'
                                  '.l(t("Yes, delete"), $base_url.'/admin/dm/delete/'.$media_id, array('attributes'=>array('class'=>'dmc-delete-it'))).'
                               </div>
                            </div>
                            <div class="save">
                               '.l(t("Save"),'javascript:void(0)',array('attributes'=>array('class'=>'save_new_data', 'onclick'=>'getDailymotionupdatedvalues();'), 'fragment' => '','external'=>true)).'
                            </div>
                         </div>
                   </div>';
        $str .= '</form>';
        $str .= '</div>';
        $str .= '</div>';
        print json_encode($str); exit;
    }
}

/**
     * Method to update Dailymotion video
     */

function update_dm_callback()
    {
        if (isset($_POST['id']) && !empty($_POST['id'])) {
            $dailymotion = new DailymotionOwnMethod();
            $videoInfo   = $dailymotion->updateDailymotionVideoData($_POST['id'], $_POST['data']);
            drupal_set_message(t('Your video was saved successfully.'));
            print json_encode(array(
                'msg' => 'Successfully updated'
            ));
            exit;
        }
    }
    
/**
     * Method to get user Dailymotion video
     */

function get_My_dm_video_callback()
{ 
    $search_title = (isset($_POST['title']) && !empty($_POST['title']))?$_POST['title']:'';
    $pageno = (isset($_POST['pageno']) && !empty($_POST['pageno']))?$_POST['pageno']:1;
    $itemsPerPage = 5;
    $dailymotion = new DailymotionOwnMethod();
    $output      = $dailymotion->getDMSideVideos('me', $fields = array(
        'id',
        'title',
        'embed_url',
        'thumbnail_url',
        'views_total',
        'duration',
        'description'
    ), (int)$pageno, $itemsPerPage, $search_title);
    print json_encode($output);exit;
}

