<?php

include_once(DRUPAL_ROOT . "/sites/all/modules/dailymotion/lib/CloudKey.php");

class DailymotionCloudOwnMethod
{
    
    //Variable for dailymotion cloud
    protected $dmc_uid;
    protected $dmc_api_key;
    
    //For create instance of daliymotion cloud
    protected $cloudkey;
    
    //For retun data
    public $returndata = array();
    
    function __construct($dmc_uid = NULL, $dmc_api_key = NULL)
    {
        $this->dmc_uid     = $dmc_uid;
        $this->dmc_api_key = $dmc_api_key;
        
        if (!empty($this->dmc_uid) && !empty($this->dmc_api_key)) {
            $this->cloudkey = new CloudKey($this->dmc_uid, $this->dmc_api_key);
        } else {
            throw new InvalidArgumentException('Invalide credential in api parameter.');
        }
        return $this;
    }
    
    /**
     * Method to update dailymotion video
     */
    public function getDailymotionCloudUserInfo()
    {
        $res = $this->cloudkey->user->info(array(
            'fields' => array(
                'id'
            )
        ));
        return isset($res->id) ? $res->id : null;
    }
    
    /**
     * Method to get connection information
     */
    public function getConnectedInformation()
    {
        $resultdata = array();
        try {
            $userdata = $this->cloudkey->user->info(array(
                "fields" => array(
                    "id",
                    "username",
                    "email",
                    "first_name",
                    "last_name"
                )
            ));
            $mediadata = $this->cloudkey->media->list(array(
                'fields' => array(
                    'id'
                )
            ));
            $lastuploaded = $this->cloudkey->media->search(array(
                "fields" => array(
                    "created"
                ),
                "query" => "SORT:-created",
                "page" => 1,
                "per_page" => 1
            ));
            $resultdata['udata']         = $userdata->first_name;
            $resultdata['mdata']         = $mediadata->total;
            $resultdata['last_uploaded'] = (isset($lastuploaded->list[0]->created)) ? date('M d, Y', $lastuploaded->list[0]->created) : 'No video uploaded yet.';
            return $resultdata;
        }
        catch (Exception $e) {
            watchdog('dailymotion', 'Function : getConnectedInformation Mesg : %msg', array('%msg' => $e->getMessage()) , WATCHDOG_ERROR);
            return $resultdata['error'] = $e->getMessage();
        }
    }
    
    /**
     * Method to delete dailymotion video
     */
    public function deleteDailymotionCloudMedia($media_id = NULL)
    {
        try {
            if (!empty($media_id)) {
                $res = $this->cloudkey->media->delete(array(
                    'id' => $media_id
                ));
                watchdog('dailymotion', 'Dailymotion Cloud video deleted successfully by @user and video ID  @video_id', array('@user' => @$GLOBALS['user']->name, '@video_id' => $media_id), WATCHDOG_INFO);
            } else {
                
            }
        }
        catch (Exception $e) {
            watchdog('dailymotion', 'Function : deleteDailymotionCloudMedia Mesg : %msg', array('%msg' => $e->getMessage()) , WATCHDOG_ERROR);
            return $resultdata['error'] = $e->getMessage();
        }
    }
    
    /**
     * Method to get Dailymotion players
     */
    public function getDailymotionCloudPlayer()
    {
        $resultdata = array();
        try {
            $res = $this->cloudkey->player_preset->list();
            if (isset($res)) {
                foreach ($res as $players) {
                    $resultdata[$players->id] = $players->name;
                }
                return $resultdata;
            }
        }
        catch (Exception $e) {
            watchdog('dailymotion', 'Function : getDailymotionCloudPlayer Mesg : %msg', array('%msg' => $e->getMessage()) , WATCHDOG_ERROR);
            return $resultdata['error'] = $e->getMessage();
        }
    }
    
    /**
     * Method to DailymotionCloud channels
     */
    public function getDailymotionCloudChannels()
    {
        try {
            return array(
                'news' => 'News &amp; Politics',
                'fun' => 'Funny',
                'shortfilms' => 'Film &amp; TV',
                'music' => 'Music',
                'auto' => 'Auto-Moto',
                'travel' => 'Travel',
                'creation' => 'Arts',
                'videogames' => 'Gaming',
                'webcam' => 'Webcam &amp; Vlogs',
                'sport' => 'Sports &amp; Extreme',
                'animals' => 'Animals',
                'people' => 'People &amp; Family',
                'tech' => 'Tech &amp; Science',
                'school' => 'College',
                'lifestyle' => 'Life &amp; Style',
                'latino' => 'Latino',
                'gaylesbian' => 'Gay &amp; Lesbian',
                'sexy' => 'Sexy',
                'tv' => 'TV'
            );
        }
        catch (Exception $e) {
            watchdog('dailymotion', 'Function : getDailymotionCloudChannels Mesg : %msg', array('%msg' => $e->getMessage()) , WATCHDOG_ERROR);
            return $resultdata['error'] = $e->getMessage();
        }
    }
    
    /**
     * Method to get dailymotion videos
     */
    public function getDailymotionCloudVideos($page_no = 1, $per_page = 10, $search_title = '', $filter = '-created')
    {
        $resultdata = array();
        try {
            if (!empty($search_title)) {
                $search_where = 'meta.title:' . $search_title;
            } else {
                $search_where = '';
            }
            $res = $this->cloudkey->media->search(array(
                'fields' => array(
                    'id',
                    'meta.title',
                    'created',
                    'stats.global.total',
                    'assets.jpeg_thumbnail_medium.stream_url',
                    'assets.source.duration'
                ),
                "query" => "$search_where SORT:$filter",
                'page' => ($page_no) ? $page_no : 1,
                'per_page' => ($per_page) ? $per_page : 10
            ));
            if (isset($res->list) && !empty($res->list)) {
                foreach ($res->list as $key => $media) {
                    $expires   = time() + (1600 * 24 * 60 * 60);
                    $embed_url = $this->cloudkey->media->get_embed_url(array(
                        'id' => $media->id,
                        'expires' => $expires
                    ));
                    
                    $resultdata['videos'][$key]['title']      = !empty($media->meta->title) ? $media->meta->title : null;
                    $resultdata['videos'][$key]['media_id']   = !empty($media->id) ? $media->id : null;
                    $resultdata['videos'][$key]['stream_url'] = !empty($media->assets->jpeg_thumbnail_medium->stream_url) ? $media->assets->jpeg_thumbnail_medium->stream_url : null;
                    $resultdata['videos'][$key]['embed_url']  = !empty($embed_url) ? $embed_url : null;
                    $resultdata['videos'][$key]['created']    = !empty($media->created) ? date('M d, Y', $media->created) : null;
                    $resultdata['videos'][$key]['total_view'] = !empty($media->stats->global->total) ? $media->stats->global->total : 0;
                    $resultdata['videos'][$key]['duration']   = !empty($media->assets->source->duration) ? $media->assets->source->duration : null;
                }
                $resultdata['total_record'] = $res->total;
                $resultdata['total_pages']  = $res->pages;
            }
            
            return ($resultdata);
        }
        catch (Exception $e) {
            watchdog('dailymotion', 'Function : getDailymotionCloudVideos Mesg : %msg', array('%msg' => $e->getMessage()) , WATCHDOG_ERROR);
            return $resultdata['error'] = $e->getMessage();
        }
    }
    
    /**
     * Method to get dailymotion cloud video detail
     */
    public function getDailymotionCloudVideosDetails($media_id = null)
    {
        $resultdata = array();
        try {
            if (!empty($media_id)) {
                $res = $this->cloudkey->media->info(array(
                    'fields' => array(
                        'id',
                        'created',
                        'assets.jpeg_thumbnail_source.stream_url'
                    ),
                    'id' => $media_id
                ));
                
                $expires   = time() + (1600 * 24 * 60 * 60);
                $embed_url = $this->cloudkey->media->get_embed_url(array(
                    'id' => $media_id,
                    'expires' => $expires
                ));
                $metadata  = $this->cloudkey->media->get_meta(array(
                    'id' => $media_id
                ));
                
                $resultdata['media_id']   = $media_id;
                $resultdata['embed_url']  = !empty($embed_url) ? $embed_url : null;
                $resultdata['stream_url'] = !empty($res->assets->jpeg_thumbnail_source->stream_url) ? $res->assets->jpeg_thumbnail_source->stream_url : null;
                
                if (isset($metadata) && !empty($metadata)) {
                    foreach ($metadata as $key => $val) {
                        if (!in_array($key, array(
                            'sharing_key',
                            'explicit',
                            'channel',
                            'author'
                        ))) {
                            $resultdata['meta'][$key] = $val;
                        }
                    }
                }
                return $resultdata;
            } else {
                
            }
        }
        catch (Exception $e) {
            watchdog('dailymotion', 'Function : getDailymotionCloudVideosDetails Mesg : %msg', array('%msg' => $e->getMessage()) , WATCHDOG_ERROR);
            return $resultdata['error'] = $e->getMessage();
        }
    }
    
    /**
     * Method to get dailymotion meta tags
     */
    public function getDailymotionCloudVideosKeywords($media_id = null)
    {
        try {
            if (!empty($media_id)) {
                
                $metadata = $this->cloudkey->media->get_meta(array(
                    'id' => $media_id
                ));
                if (isset($metadata) && !empty($metadata)) {
                    foreach ($metadata as $key => $val) {
                        if (in_array($key, array('keywords'))) {
                            return $val;
                        }
                    }
                } else {
                    return '';
                }
                
            } else {
                
            }
        }
        catch (Exception $e) {
            watchdog('dailymotion', 'Function : getDailymotionCloudVideosKeywords Mesg : %msg', array('%msg' => $e->getMessage()) , WATCHDOG_ERROR);
            return $resultdata['error'] = $e->getMessage();
        }
    }
    
    /**
     * Method to update dailymotion cloud meta tags
     */
    public function setDailyMotionCloudVideoMetas($media_id = null, $args = array())
    {
        try {
            if (!empty($media_id) && !empty($args)) {
                $metadata = $this->cloudkey->media->set_meta(array(
                    'id' => $media_id,
                    'meta' => $args
                ));
                watchdog('dailymotion', 'Dailymotion Cloud video metas updated successfully by @user and video ID  @video_id', array('@user' => @$GLOBALS['user']->name, '@video_id' => $media_id), WATCHDOG_INFO);
                return $metadata;
            } else {
                
            }
        }
        catch (Exception $e) {
            watchdog('dailymotion', 'Function : setDailyMotionCloudVideoMetas Mesg : %msg', array('%msg' => $e->getMessage()) , WATCHDOG_ERROR);
            return $resultdata['error'] = $e->getMessage();
        }
    }
    
    /**
     * Method to delete dailymotion cloud video metatags
     */
    public function removeDailymotionCloudVideoMetas($media_id = null, $args = array())
    {
        try {
            if (!empty($media_id) && !empty($args)) {
                $metadata = $this->cloudkey->media->remove_meta(array(
                    'id' => $media_id,
                    'keys' => $args
                ));
                watchdog('dailymotion', 'Dailymotion Cloud video metas deleted successfully by @user and video ID  @video_id', array('@user' => @$GLOBALS['user']->name, '@video_id' => $media_id), WATCHDOG_INFO);
            } else {
                
            }
        }
        catch (Exception $e) {
            watchdog('dailymotion', 'Function : removeDailymotionCloudVideoMetas Mesg : %msg', array('%msg' => $e->getMessage()) , WATCHDOG_ERROR);
            return $resultdata['error'] = $e->getMessage();
        }
    }
    
    /**
     * Method to update dailymotion cloud video thumbnail
     */
    public function setDailyMotionCloudVideoThumbnail($media_id = null, $url = NULL)
    {
        try {
            if (!empty($media_id) && !empty($url)) {
                $metadata = $this->cloudkey->media->set_thumbnail(array(
                    'id' => $media_id,
                    'url' => $url
                ));
                watchdog('dailymotion', 'Dailymotion Cloud video thumbnail updated successfully by @user and video ID  @video_id', array('@user' => @$GLOBALS['user']->name,  '@video_id' => $media_id), WATCHDOG_INFO);
            } else {
                
            }
        }
        catch (Exception $e) {
            watchdog('dailymotion', 'Function : setDailyMotionCloudVideoThumbnail Mesg : %msg', array('%msg' => $e->getMessage()) , WATCHDOG_ERROR);
            return $resultdata['error'] = $e->getMessage();
        }
    }
    
    /**
     * Method to upload dailymotion cloud video
     */
    public function uploadDailymotionCloudVideo($video_url = null, $file_name = null)
    {
        if (!empty($video_url) && file_exists($video_url) && !empty($file_name)) {
            $res        = $this->cloudkey->file->upload_file($video_url);
            $source_url = $res->url;
            $assets     = array(
                'mp4_h264_aac',
                'mp4_h264_aac_hq',
                'jpeg_thumbnail_medium',
                'jpeg_thumbnail_source'
            );
            $meta       = array(
                'title' => $file_name
            );
            $media      = $this->cloudkey->media->create(array(
                'assets_names' => $assets,
                'meta' => $meta,
                'url' => $source_url
            ));
            watchdog('dailymotion', 'Dailymotion Cloud video uploaded successfully by @user and video ID  @video_id', array('@user' => @$GLOBALS['user']->name, '@video_id' => $media->id), WATCHDOG_INFO);
            return $media;
        }
    }
    
}