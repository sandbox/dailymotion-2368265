<?php
   $id = "video_id1"; 
   $svalue = ""; 
   $multiple = true; 
   ?>
<?php if (empty($GLOBALS['CLOUD_CON_INFO']) && empty($GLOBALS['DAILYMOTION_CON_INFO'])) : ?>
<div id="full_wrapper" class="wrap dm-common">
   <div class="main-gallery-container">
      <div class="infomation_form" id="dailymotion_cloud_div">
         <div class="wrap">
            <div class="dmc-not-auth">
               <div class="icon"></div>
               <div class="msg">
                  <p><?php print t('You are not connected to an account on Dailymotion cloud.'); ?></p>
                  <p><?php print t('In order to see your videos here, go to the plug-in setting to connect your Dailymotion Cloud account.'); ?></p>
               </div>
               <div class="link"><a href="<?php print  url('admin/dm/config_setting', array('absolute' => TRUE)); ?>"><?php print t('Go to Settings'); ?></a></div>
            </div>
         </div>
      </div>
   </div>
</div>
<?php elseif (!empty($GLOBALS['CLOUD_CON_INFO']) || !empty($GLOBALS['DAILYMOTION_CON_INFO'])) : ?>
<div id="full_wrapper" class="wrap dm-common">
   <div class="overlay"></div>
   <div class="upload_message"></div>
   <div class="filelist"></div>
   <div class="plupload-thumbs <?php if ($multiple): ?>plupload-thumbs-multiple<?php endif; ?>" id="<?php echo $id; ?>plupload-thumbs"></div>
   <div class="action_wrappers">
      <label>Upload videos in :</label>
      <select id="account_name" name="account_name">
         <?php if (!empty($GLOBALS['CLOUD_CON_INFO'])) : ?>
         <option value="cloud"><?php print t('Dailymotion Cloud'); ?></option>
         <?php endif; ?>
         <?php if (!empty($GLOBALS['DAILYMOTION_CON_INFO'])) : ?>
         <option value="dailymotion"><?php print t('Dailymotion.com'); ?></option>
         <?php endif; ?>
      </select>
      <a href="#" id="submitPattern"><?php print t('Upload Videos'); ?></a>
   </div>
   <div id="plupload-upload-ui">
      <input type="hidden" name="<?php echo $id; ?>" id="<?php echo $id; ?>" value="<?php echo $svalue; ?>" />
      <div class="drag-drop-area plupload-upload-uic hide-if-no-js <?php if ($multiple): ?>plupload-upload-uic-multiple<?php endif; ?>" id="<?php echo $id; ?>plupload-upload-ui">
         <div class="drop_wrapper">
            <p><?php print t('Drag and drop your video here'); ?></p>
            <p><?php print t('or'); ?></p>
            <input id="<?php echo $id; ?>plupload-browse-button" type="button" value="<?php print t('Browse files to upload'); ?>" class="button" />
         </div>
         <span class="ajaxnonceplu" id="ajaxnonceplu<?php //echo wp_create_nonce($id . 'pluploadan'); ?>"></span>
      </div>
   </div>
   <div class="clear"></div>
   <ul class="upload_desc">
      <li>- <?php print t('Your server maximum upload file size is @size. If you want to increase this limit please change in your server php.ini file.', array('@size' => fileSizeConvert(file_upload_max_size()))); ?></li>
      <li>- <?php print t('Recommended formats: mp4 (H264), mov, wmv, avi'); ?></li>
      <li>- <?php print t('Recommended resolution: 640x480, 1280x720 or 1920x1080'); ?></li>
      <li>- <?php print t('Recommended frequency: 25 frames per second'); ?></li>
   </ul>
</div>
<a href="#dm-cloud-edit-form" id="editpopup"></a>
<div class="dmc-edit-form-main">
   <div id="dm-cloud-edit-form" class="dm-cloud-edit-form"></div>
</div>
<?php endif; ?>