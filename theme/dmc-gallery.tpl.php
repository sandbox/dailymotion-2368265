<?php
   global $base_url;
   $output = $variables['result'];//echo "<pre>"; print_r($output);
   $search_title = $variables['search_title'];
   $pageno = $variables['pageno'];
   $itemPerPage = 6;
   ?>
<div class="dmc-main dm-main dm-common">
   <?php if (!empty($GLOBALS['CLOUD_CON_INFO']['DMC_USER_ID']) && !empty($GLOBALS['CLOUD_CON_INFO']['DMC_API_KEY'])) : ?>
   <div class="overlay"></div>
   <div class="dm-loading"><img src="<?php print DAILYMOTION_FULL_URL; ?>/img/495.GIF" alt="Loading..." /></div>
   <!------------ Header--Start-------------->
   <?php if (isset($output['videos']) || !empty($search_title)): ?>
   <div class="header">
      <div class="upload-link"><?php print l('Upload videos', 'admin/dm/video_upload'); ?></div>
      <div class="filter">
         <?php print drupal_render(drupal_get_form('dmc_filter_form'));?>
         <?php
            if ($search_title != '') {
               if (isset($output['total_record'])) {
                  $found = $output['total_record'];
               } else {
                  $found = 0;
               }
               print '<div class="search-result-head"><div class="back-link">'.l('<< Back to list', '/admin/dm/video-gallery').'</div><div class="result-count">'.$found.' results for <i>'.$search_title.'</i></div></div>';
            }
            ?>
      </div>
      <div class="paging">
         <span class="total-records italic"><?php print $output['total_record']; ?> items</span>
         <?php
            if (isset($output['total_record']) && $output['total_record'] > $itemPerPage) {
               pager_default_initialize($output['total_record'], $itemPerPage, $element = 0);
               print theme('pager', array('quantity' => 4));
            }
            ?>
      </div>
   </div>
   <?php endif;?>
   <!------------ Header--Ends-------------->
   <!------------ Content--Start-------------->
   <div class="content">
      <?php if (isset($output['videos']) && count($output['videos']) > 0) { ?>
      <table class = "video-gallery-container" cellpadding = "0" cellspacing = "0">
         <?php
            foreach ($output['videos'] as $video):
            $src = (isset($video['stream_url'])) ? $video['stream_url'] : DAILYMOTION_FULL_URL . '/img/no_files_found.jpg';
            $title = (strlen($video['title']) > 167)?substr($video['title'], 0, 167) . '...':$video['title'];
            ?>
         <tr class="dmc-gallery-rows">
            <td class="first-column"><img title="<?php print $video['title'];?>" alt="<?php print $video['embed_url'] ;?>" class="video-thumbnail" src="<?php print $src ;?>" alt="" /></td>
            <td class="second-column">
               <div class="title"><?php print $title;?></div>
               <div class="views italic"><?php print $video['total_view'];?> views</div>
               <div class="hide-option">
                  <?php print l("Edit",'javascript:void(0)',array('attributes'=>array('class'=>'edit-trigger', 'onclick'=>'editDMCvideo(\'' . $video['media_id'] . '\');'), 'fragment' => '','external'=>true));?>
                  <?php print l("Trash",'javascript:void(0)',array('attributes'=>array('class'=>'trash-trigger'), 'fragment' => '','external'=>true));?>
                  <div class="confirm-box">
                     <div class="head"><span class="arrow"></span><?php print t('Delete this video?'); ?></div>
                     <div class="message"><?php print t('This video will be deleted from your Dailymotion Cloud account.');?></div>
                     <?php print l("No, keep it",'javascript:void(0)',array('attributes'=>array('class'=>'keep-it'), 'fragment' => '','external'=>true));?>
                     <?php print l("Yes, delete",'admin/dmc/delete/'.$video['media_id'], array('attributes'=>array('class'=>'delete-it')));?>
                  </div>
                  <?php print l("View",'javascript:void(0)',array('attributes'=>array('class'=>'view-trigger'), 'fragment' => '','external'=>true));?>
               </div>
            </td>
            <td class="third-column">
               <div class="date"><?php print $video['created'];?></div>
               <div class="other-backend-info">
                  <div id="duration"><?php print $video['duration'];?></div>
               </div>
            </td>
         </tr>
         <?php endforeach;?>
      </table>
      <?php } elseif ($search_title != ''){
         print '<div class="no-result-main">
                           <div class="inner"></div>
                           <div class="msg-line-one">'.t('No videos found for').' <span class="italic">'. $search_title .'.</span><span class="new-search">'.t('Try a new search.').'</span></div>
                        </div>';
         } else {
            print '<div class="no-uploaded-main">
                           <div class="inner"></div>
                           <div class="msg-line-one">
                              <p>'.t('You have not uploaded any video yet.').'</p>
                              <p>'.t('Start uploading your videos now!').'</p>
                           </div>
                           '.l('Upload videos', '/admin/dm/video_upload').'
                        </div>';
         }
         ?>
   </div>
   <!------------ Content--Edns-------------->
   <!------------ Footer--Start-------------->
   <?php if (isset($output['total_record']) && $output['total_record'] > $itemPerPage):?>
   <div class="footer">
      <div class="paging">
         <span class="total-records italic"><?php print $output['total_record']; ?> items</span>
         <?php
            pager_default_initialize($output['total_record'], $itemPerPage, $element = 0);
            print theme('pager', array('quantity' => 4));
            ?>
      </div>
   </div>
   <?php endif;?>
   <?php else : ?>
   <div class="dmc-not-auth">
      <div class="icon"></div>
      <div class="msg">
         <p><?php print t('You are not connected to an account on Dailymotion cloud.');?></p>
         <p><?php print t('In order to see your videos here, go to the plug-in setting to connect your Dailymotion Cloud account.');?></p>
      </div>
      <div class="link"><?php print l("Go to Settings", 'admin/dm/config_setting'); ?></div>
   </div>
   <?php endif; ?>
   <!------------ Footer--Ends-------------->
</div>