<?php
   global $base_url;
   $output = $variables['result'];//echo "<pre>"; print_r($output);
   $selected = $variables['selected'];
   $video_status = $variables['status'];
   $search_title = $variables['search_title'];
   $pageno = $variables['pageno'];
   $connection = variable_get('dailymotion_authentication_info');
   ?>
<div class="dm-main dm-common">
   <?php if (!empty($connection)): ?>
   <div class="overlay"></div>
   <div class="dm-loading"><img src="<?php print DAILYMOTION_FULL_URL; ?>/img/495.GIF" alt="Loading..." /></div>
   <!------------ Header--Start-------------->
   <?php if (isset($output['videos']) || !empty($search_title)): ?>
   <div class="header">
      <div class="upload-link"><?php print l('Upload videos', 'admin/dm/video_upload'); ?></div>
      <div class="filter">
         <?php print drupal_render(drupal_get_form('dm_filter_form'));?>
         <?php
            if ($search_title != ''):
               print '<div class="search-result-head"><div class="back-link">'.l('<< Back to list', 'admin/dm/video-gallery/dailymotion').'</div><div class="result-count">Results for <i>'.$search_title.'</i></div></div>';
            endif;
            ?>
      </div>
      <div class="paging">
         <?php
            $next = '';
            $previous = '';
               if ($pageno > 1):
               $previous = $pageno - 1;
               print l('Previous', $base_url.'/admin/dm/video-gallery/dailymotion', array(
                                                                             'attributes' => array('class'=> 'previous'),
                                                                             'query'=>array(
                                                                                            'pageno' => $previous,
                                                                                            'dm_video_title' => $search_title,
                                                                                            'filter' => $selected,
                                                                                            'status' => $video_status
                                                                                            )));
            endif;
            if (isset($output['has_more']) && $output['has_more'] == 1):
               $next = $pageno + 1;
               print l('Next', $base_url.'/admin/dm/video-gallery/dailymotion', array(
                                                                             'attributes' => array('class'=> 'next'),
                                                                             'query'=>array(
                                                                                            'pageno' => $next,
                                                                                            'dm_video_title' => $search_title,
                                                                                            'filter' => $selected,
                                                                                            'status' => $video_status
                                                                                            )));
            endif;
            ?>
      </div>
   </div>
   <?php endif;?>
   <!------------ Header--Ends-------------->
   <!------------ Content--Start-------------->
   <div class="content">
      <?php if (isset($output['videos']) && count($output['videos']) > 0) { ?>
      <table class = "video-gallery-container" cellpadding = "0" cellspacing = "0">
         <?php
            foreach ($output['videos'] as $video):
            $src = (isset($video['thumbnail_url'])) ? $video['thumbnail_url'] : DAILYMOTION_FULL_URL . '/img/no_files_found.jpg';
            $title = (strlen($video['title']) > 167)?substr($video['title'], 0, 167) . '...':$video['title'];
            $tags = (count($video['tags']) > 0 && isset($video['tags']))?implode(", ", $video['tags']):'';
            $channel = (!empty($video['channel.name']))?$video['channel.name'] . ' - ' : '';
            $status = (isset($video['private']) && $video['private'] == 1)?"Private":"Public";
            $overlayClasspp = (isset($status) && $status == 'Private')?'<div class="overlay-container"><div class="privateOverlay"></div></div>':'';
            $publish = (isset($video['published']) && $video['published'] == 1)?"published":"unpublished";
            $overlayClasspu = (isset($publish) && $publish == 'unpublished')?'<div class="private-container"><div class="unpublishedOverlay"></div></div>':'';
            ?>
         <tr class="dm-gallery-rows">
            <td class="first-column"><?php print $overlayClasspp; ?> <?php print $overlayClasspu; ?><img title="<?php print $video['title'];?>" alt="<?php print $video['embed_url'] ;?>" class="video-thumbnail" src="<?php print $src ;?>" alt="" /></td>
            <td class="second-column">
               <div class="title"><?php print $title;?></div>
               <div class="private-status"><?php if ($status == 'Private'):print '- '.$status;endif;?></div>
               <div class="views-channel">
                  <span class="channel"><?php print $channel;?></span>
                  <span class="views italic"><?php print $video['views_total'];?> views</span>
                  <?php if ($publish == 'unpublished'):?>
                              <span class="unpublished-tooltip italic">, unpublished <span class="qus_mark tooltip"><span><img class="callout" src="<?php print DAILYMOTION_URL; ?>/img/tool-tip-arrow.png" />A video can only be published when it has both a title and a channel assigned to it.</span></span></span>
                  <?php endif; ?>
               </div>
               <div class="hide-option">
                  <?php print l("Edit",'javascript:void(0)',array('attributes'=>array('class'=>'edit-trigger', 'onclick'=>'editDailymotionvideo(\'' . $video['id'] . '\');'), 'fragment' => '','external'=>true));?>
                  <?php print l("Trash",'javascript:void(0)',array('attributes'=>array('class'=>'trash-trigger'), 'fragment' => '','external'=>true));?>
                  <div class="confirm-box">
                     <div class="head"><span class="arrow"></span><?php print t('Delete this video?'); ?></div>
                     <div class="message"><?php print t('This video will be deleted from your Dailymotion.com account.');?></div>
                     <?php print l("No, keep it",'javascript:void(0)',array('attributes'=>array('class'=>'keep-it'), 'fragment' => '','external'=>true));?>
                     <?php print l("Yes, delete",'admin/dm/delete/'.$video['id'], array('attributes'=>array('class'=>'delete-it')));?>
                  </div>
                  <?php print l("View",'javascript:void(0)',array('attributes'=>array('class'=>'view-trigger'), 'fragment' => '','external'=>true));?>
               </div>
            </td>
            <td class="third-column">
               <div class="date"><?php print date('M d, Y', $video['created_time']);?></div>
               <div class="tags"><?php print $tags; ?></div>
               <div class="other-backend-info">
                  <div id="desc"><?php print $video['description'];?></div>
                  <div id="video_status"><?php print $status; ?></div>
                  <div id="duration"><?php print $video['duration'];?></div>
               </div>
            </td>
         </tr>
         <?php endforeach;?>
      </table>
      <?php } elseif ($search_title != ''){
         print '<div class="no-result-main">
                           <div class="inner"></div>
                           <div class="msg-line-one">'.t('No videos found for').' <span class="italic">'. $search_title .'.</span><span class="new-search">'.t('Try a new search.').'</span></div>
                        </div>';
         } else {
            print '<div class="no-uploaded-main">
                           <div class="inner"></div>
                           <div class="msg-line-one">
                              <p>'.t('You have not uploaded any video yet.').'</p>
                              <p>'.t('Start uploading your videos now!').'</p>
                           </div>
                           '.l('Upload videos', '/admin/dm/video_upload').'
                        </div>';
         }
         ?>
   </div>
   <!------------ Content--Edns-------------->
   <!------------ Footer--Start-------------->
   <?php if (($pageno > 1) || isset($output['has_more']) && $output['has_more'] == 1):?>
   <div class="footer">
      <div class="paging">
         <?php
            if ($pageno > 1):
            $previous = $pageno - 1;
            print l('Previous', $base_url.'/admin/dm/video-gallery/dailymotion', array(
                                                                          'attributes' => array('class'=> 'previous'),
                                                                          'query'=>array(
                                                                                         'pageno' => $previous,
                                                                                         'dm_video_title' => $search_title,
                                                                                         'filter' => $selected,
                                                                                          'status' => $video_status
                                                                                         )));
            endif;
            
            if (isset($output['has_more']) && $output['has_more'] == 1):
            $next = $pageno + 1;
            print l('Next', $base_url.'/admin/dm/video-gallery/dailymotion', array(
                                                                          'attributes' => array('class'=> 'next'),
                                                                          'query'=>array(
                                                                                         'pageno' => $next,
                                                                                         'dm_video_title' => $search_title,
                                                                                         'filter' => $selected,
                                                                                          'status' => $video_status
                                                                                         )));
            endif;
            ?>
      </div>
   </div>
   <?php endif;?>
    <?php else : ?>
   <div class="dmc-not-auth">
      <div class="icon"></div>
      <div class="msg">
         <p><?php print t('You are not connected to an account on Dailymotion.com.');?></p>
         <p><?php print t('In order to see your videos here, go to the plug-in setting to connect your Dailymotion account.');?></p>
      </div>
      <div class="link"><?php print l("Go to Settings", 'admin/dm/config_setting'); ?></div>
   </div>
   <?php endif; ?>
   <!------------ Footer--Ends-------------->
</div>