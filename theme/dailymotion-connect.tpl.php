<?php
   $dailymotionData = '';
   if(!empty($GLOBALS['DAILYMOTION_APP_SECRET_INFO'])) {
        $dailymotionData = conectionDailymotion();
   }
   ?>
<div id="header-container" class="wrap dm-common">
   <?php if(!empty($GLOBALS['CLOUD_CON_INFO']) || !empty($GLOBALS['DAILYMOTION_CON_INFO'])) : ?>
   <p><?php  print t('Congrats, you\'re now connected!'); ?><br/>
      <?php  print t('You can start managing your videos using the Dailymotion menus to the left.'); ?>
   </p>
   <?php else : ?>
   <p><?php  print t('Insert your video into your post while you writing.'); ?><br/>
      <?php  print t('Connect Your Dailymotion account(s) below.'); ?>
   </p>
   <?php endif; ?>
   <div class="box_wrapper">
      <?php if(isset($dailymotionData) && !empty($dailymotionData) && is_array($dailymotionData)) : ?>
      <div id="dailymotion_box">
         <div class="align_center">
            <div class="header_logo first"><img src="<?php print DAILYMOTION_FULL_URL; ?>/img/dm_head.jpg" alt="" /></div>
            <div id="dailymotion_box_conected">
               <div class="right_arraow"><img src="<?php print DAILYMOTION_FULL_URL; ?>/img/right_sign.jpg" alt="" /></div>
               <div class="connected"><?php print t('Your Dailymotion account is connected'); ?></div>
            </div>
            <div id="dailymotion_box_conected" class="conected_second">
               <div class="account_picture"><img src="<?php print $dailymotionData['user_photo']; ?>" alt="" /></div>
               <div class="account_name"><?php print $dailymotionData['screenname']; ?></div>
               <div class="disconnect_wrapper">
                  <?php print l(t('Disconnect'), 'dailymotion-disconnect/dailymotion', array('attributes' => array('class' => array('disconnect_account'), 'rel' => 'dailymotion'))); ?>
               </div>
               <div class="total_video">
                  <div><?php print t('Total videos'); ?></div>
                  <div class="span_count"><?php  print $dailymotionData['total_record']; ?></div>
               </div>
               <div class="total_video">
                  <div><?php print t('Last uploaded'); ?></div>
                  <div class="span_count"><?php  print $dailymotionData['last_uploaded']; ?></div>
               </div>
            </div>
         </div>
         <div id="header-container" class="wrap user_publish_setting">
            <div class="wrap">
               <h2><?php print t('Publication Settings'); ?></h2>
               <?php print drupal_render(drupal_get_form('dailymotion_plublication_form')); ?>
            </div>
         </div>
      </div>
      <?php else: ?>
      <div id="dailymotion_box">
         <div class="align_center">
            <div class="header_logo first"><img src="<?php print DAILYMOTION_FULL_URL; ?>/img/dm_head.jpg" alt="" /></div>
            <?php if(isset($GLOBALS['DAILYMOTION_APP_SECRET_INFO']) && !empty($GLOBALS['DAILYMOTION_APP_SECRET_INFO'])) : ?>
            <a class="dm_pop_btn" id="" href="<?php print $dailymotionData; ?>"><?php print t('Connect to Dailymotion'); ?></a>
            <?php else : ?>
            <a class="dm_pop_btn" id="dm_auth_popup" href="#dailymotion_form_popup"><?php print t('Connect to Dailymotion'); ?></a>
            <?php endif; ?>
            <a class="sub_link" id="sub_link" target="_balnk" href="#dailymotion_form_popup"><?php print t('or create a dailymotion account'); ?></a>
         </div>
      </div>
      <div style="display: none">
         <div id="dailymotion_form_popup" class="dm-common" style="float:left;">
            <div class="connect_heading"><?php print t('Connect to Dailymotion.com'); ?></div>
            <div class="wrap11">
               <?php print drupal_render (drupal_get_form('dailymotion_configuration_form')); ?>
               <div class="create_account_desc">
                  <h3><?php print t('How to create an API Key'); ?></h3>
                  <ol>
                     <li><?php print t('Connect with your Dailymotion account on'); ?> <br>
                        <a target="_balnk" href="http://www.dailymotion.com/profile/developer">http://www.dailymotion.com/profile/developer</a>
                     </li>
                     <li><?php print t('If necessary, click on the button "Create a new API Key" to create a new form'); ?> </li>
                     <li>
                        <?php print t('Enter the following info in the form:'); ?> <br>
                        <ul>
                           <li>Name of you app: "My wordpress/Drupal app"</li>
                           <li>Application website url: <a href="javascript:void(0);"><?php print url('<front>', array('absolute' => true)) ?></a></li>
                           <li>Callback url: <a href="javascript:void(0);"><?php print url('admin/dm/config_setting', array('absolute' => true)) ?></a></li>
                        </ul>
                     </li>
                     <li>Save the form</li>
                     <li>Copy your API key and secret in the <a href="javascript:void(0);" class="hide_account_desc">Form here</a></li>
                  </ol>
               </div>
            </div>
         </div>
      </div>
      <?php endif; ?>
      <?php if (!empty($GLOBALS['CLOUD_CON_INFO']['DMC_USER_ID']) && !empty($GLOBALS['CLOUD_CON_INFO']['DMC_API_KEY'])) : ?>
      <div id="dailymotion_box" class="dmc-box">
         <div class="align_center">
            <div class="header_logo"><img src="<?php print DAILYMOTION_FULL_URL; ?>/img/dm_cloud.jpg" alt="" /></div>
            <div id="dailymotion_box_conected">
               <div class="right_arraow"><img src="<?php print DAILYMOTION_FULL_URL; ?>/img/right_sign.jpg" alt="" /></div>
               <div class="connected"><?php print t('Your Dailymotion Cloud account is connected'); ?></div>
            </div>
            <div id="dailymotion_box_conected" class="conected_second">
               <?php $data = showCloudaccountData($GLOBALS['CLOUD_CON_INFO']['DMC_USER_ID'], $GLOBALS['CLOUD_CON_INFO']['DMC_API_KEY']); ?>
               <div class="account_name"><?php print $data['udata']; ?></div>
               <div class="disconnect_wrapper">
                  <?php print l(t('Disconnect'), 'dailymotion-disconnect/cloud', array('attributes' => array('class' => array('disconnect_account'), 'rel' => 'cloud'))); ?>
               </div>
               <div class="total_video">
                  <div><?php print t('Total videos'); ?></div>
                  <div class="span_count"><?php  print $data['mdata']; ?></div>
               </div>
               <div class="total_video">
                  <div><?php print t('Last uploaded'); ?></div>
                  <div class="span_count"><?php  print $data['last_uploaded']; ?></div>
               </div>
            </div>
         </div>
      </div>
      <?php else : ?>
      <div id="dailymotion_box" class="cloudbox">
         <div class="align_center">
            <div class="header_logo"><img src="<?php print DAILYMOTION_FULL_URL; ?>/img/dm_cloud.jpg" alt="" /></div>
            <a id="cloud_form_link" class="cloud_pop_btn" href="#cloud_form_popup"><?php print t('Connect your Dailymotion Cloud account'); ?></a>
            <a class="sub_link" target="_balnk" href="https://www.dmcloud.net/"><?php print t('or start a free trail with Dailymotion Cloud'); ?></a>
         </div>
      </div>
      <div style="display: none">
         <div id="cloud_form_popup" class="dm-common" style="float:left;">
            <div class="connect_heading"><?php print t('Connect to Dailymotion Cloud'); ?></div>
            <div class="wrap11">
               <?php print drupal_render (drupal_get_form('dailymotion_cloud_configuration_form')); ?>
            </div>
         </div>
      </div>
      <?php endif; ?>
   </div>
</div>